<?php

/*
 *	Usage: get.php?doc=urlinstructions
 *	Usage: get.php?doc=urltemplate
 *	Usage: get.php?doc=urlupreport&jobid=12345
 */

$filenames = array(
'urlinstructions' => 'URL Document Uploader instructions.docx',
'urltemplate' => 'URL Document Uploader Template 02292012.xlsx',
'urlupreport' => 'report.csv'
);

$mimetypes = array(
'urlinstructions' => 'application/msword',
'urltemplate' => 'application/vnd.ms-excel',
'urlupreport' => 'text/csv'
);

$key = $_GET['doc'];
if (array_key_exists($key, $filenames)) {
	header('Content-Type: ' . $mimetypes[$key]);
	header('Content-Disposition: attachment; filename="' . $filenames[$key] . '"');
	$fname = $filenames[$key];
	if ($key == "urlupreport") {
		$jobid = $_GET['jobid'];
		$fname = "/var/www/html/tmp/urluploader/$jobid/$fname";
	}
	readfile($fname);
}

?>
