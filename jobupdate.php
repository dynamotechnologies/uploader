<?php
/*
 *	Update the timestamp on a job
 *
 *	Submit the jobid as a POST parameter
 *
 *	Response is "0" if the job is open and the update succeeded
 *
 *	Otherwise return a descriptive string error
 *	(would probably be more useful if the error had codes to indicate
 *	that the job was closed, network error, etc.)
 */

require_once("/var/www/config/uploader-conf.php");

$rechost = FILERECORDERHOST;

if ($rechost === "") {
        echo "jobupdate failed, please configure uploader";
        exit(0);
}

/*
 * Get jobid from _POST
 * Get IP addr from _SERVER
 * POST new message to FILERECORDERHOST/filerecorder/updateJob.php
 * return response
 * (0 for OK, anything else is an error)
 */

$jobid = $_POST['jobid'];
$ipaddr = $_SERVER['REMOTE_ADDR'];

$postURL = "https://$rechost/filerecorder/updateJob.php";

$postdata = http_build_query(
  array(
    'jobid' => $jobid,
    'reqip' => $ipaddr
  )
);

$opts = array('http' =>
  array(
    'method' => 'POST',
    'header' => 'Content-type: application/x-www-form-urlencoded',
    'content' => $postdata
  )
);

$context = stream_context_create($opts);

$result = file_get_contents($postURL, false, $context);

print $result;
?>
