<?php

error_reporting(0);

// Target for uploader
// print "Referrer script: " . basename($_SERVER['HTTP_REFERER']) . "\n";

define("STATUS_OK", 0);
define("STATUS_ERR", 1);

require_once "/var/www/config/uploader-conf.php";

if ((FILERECORDERHOST === "") || (BROKERDIR === "")) {
  sendResponse(STATUS_ERR, "swftarget failed, please configure uploader");
  exit(0);
}

/*
 * Globals
 */
$TMPDIR = "uploaded";	// Persistent storage dir for temporary files
$RECHOST = FILERECORDERHOST;
require_once BROKERDIR . "/includes/consts.php";
require_once BROKERDIR . "/proxies/drmProxy.php";

/*
 * DEBUG: Write POST data to file
$postdataStr = print_r($_POST, True);
$fd = fopen("${TMPDIR}/lastRequest.postdata", "wb");
if ($fd === FALSE) {
  sendResponse(STATUS_ERR, "Could not open ${TMPDIR}/lastRequest.metadata");
  exit(0);
}
fwrite($fd, $postdataStr);
fclose($fd);
 */

/*
 * Debugging function -- disable this for production code
 */
function simulateError() {
  $filename = $_FILES['Filedata']['name'];
  // Look for a substring in filename
  // that looks like _err#_
  if (preg_match('/_err([0-9]+)_/', $filename, $matches)) {
    return intval($matches[1]);
  } else {
    return UPLOAD_ERR_OK;
  }
}

function sendResponse($code, $message) {
/*
 * DEBUG: Write response to log
global $TMPDIR;
$fd = fopen("${TMPDIR}/swftarget.log", "a+");
$msg = json_encode(array(
    'code' => intval($code),
    'message' => strval($message)));
fwrite($fd, "$msg\n");
fclose($fd);
 */

  # $response = "${code} - ${message}";
  # print $response;
  print json_encode(array(
    'code' => intval($code),
    'message' => strval($message)));
}

/*
 *	Code starts here
 */

function main() {
  global $TMPDIR, $RECHOST;
  /*
   *	Validate required input
   */
  if (!isset($_FILES['Filedata'])) {
    sendResponse(STATUS_ERR, "File not submitted!?  Problem with file submission");
    exit(0);
  }

  $required_postvars = array(
  	'jobid',
  	'docdate',
  	'appsysid',
  	'appdoctype',
  	'Filename',
  	'author',
  	'projectid',
  	'datelabel');
  	// 'title',	# From $_FILES, unless overridden by $_POST
  	// 'datecreated',	# Flash will not submit these vars
  	// 'docauthor',		# if their values are null strings
  	// 'docsummary',	# (bug?)
  	// 'addressee'
  $missing_varlist = array();
  foreach ($required_postvars as $varname) {
  	if (!isset($_POST[$varname])) {
  		$missing_varlist[] = $varname;
  	}
  }
  if (count($missing_varlist) > 0) {
    sendResponse(STATUS_ERR, "Missing POST vars: " . implode(", ", $missing_varlist));
    exit(0);
  }

  $time = time();

  $jobid = $_POST['jobid'];

  $uploadData = $_FILES['Filedata'];

  $errCode = $uploadData['error'];
  // if ($errCode == UPLOAD_ERR_OK) { $errCode = simulateError(); }	// DEBUG

  if ($errCode != UPLOAD_ERR_OK) {
    $errMsg = "Error message goes here";
    switch ($errCode) {
      case UPLOAD_ERR_INI_SIZE:
        $errMsg = "The uploaded file exceeds the upload_max_filesize directive (" .
          ini_get("upload_max_filesize") . ") in php.ini.";
        break;
      case UPLOAD_ERR_FORM_SIZE:
        $errMsg = "The uploaded file exceeds the MAX_FILE_SIZE directive that " .
          "was specified in the HTML form.";
        break;
      case UPLOAD_ERR_PARTIAL:
        $errMsg = "The uploaded file was only partially uploaded.";
        break;
      case UPLOAD_ERR_NO_FILE:
        $errMsg = "No file was uploaded.";
        break;
      case UPLOAD_ERR_NO_TMP_DIR:
        $errMsg = "Missing a temporary folder.";
        break;
      case UPLOAD_ERR_CANT_WRITE:
        $errMsg = "Failed to write file to disk";
        break;
      default:
        $errMsg = "Unrecognized error code ${errCode}";
    }
    sendResponse(STATUS_ERR, $errMsg);
    exit(0);
  }

  $fileName = $uploadData['name'];
  $size = $uploadData['size'];
  $tmpFileName = $uploadData['tmp_name'];

  $title = "";
  if (array_key_exists('title', $_POST)) {
      $title = $_POST['title'];
  } else {
      $title = $fileName;
      if (preg_match('/(.*)\.[a-zA-Z0-9]{3,4}/', $fileName, $matches)) {
          $title = $matches[1];
      }
  }

  $pubdate = date("Ymd\TH:i:s", $time);		// YYYYMMDDTHH:MM:SS

  // docdate is the user-specified "date of upload"
  // and is used in the Solr/Lucene indexer
  // User specifies docdate as MM/DD/YYYY, with no time component
  $tm_ar = strptime($_POST['docdate'], '%m/%d/%Y');
  $ddtime = mktime(0, 0, 0, $tm_ar["tm_mon"] + 1, $tm_ar["tm_mday"],
    $tm_ar["tm_year"] + 1900);
  $docdate = date("Y-m-d\TH:i:s\Z", $ddtime);	// YYYY-MM-DDTHH:MM:SSZ

  $metadata = array();
  $metadata['appsysid'] = $_POST['appsysid'];
  $metadata['appdoctype'] = $_POST['appdoctype'];
  $metadata['filename'] = $fileName; // $_POST['Filename'];
  $metadata['title'] = $title;
  $metadata['author'] = $_POST['author'];
  $metadata['pubdate'] = $pubdate;

  $moreMetadata = array();
  $moreMetadata['projectid'] = $_POST['projectid'];
  $moreMetadata['docdate'] = $docdate;
  $moreMetadata['datelabel'] = $_POST['datelabel'];
  if (isset($_POST['datecreated'])) {
    $moreMetadata['datecreated'] = $_POST['datecreated'];
  } else {
    $moreMetadata['datecreated'] = "";
  }
  if (isset($_POST['level2id']) && isset($_POST['level2name'])) {
    $moreMetadata['level2name'] = $_POST['level2name'];
    $moreMetadata['level2id'] = $_POST['level2id'];
  }
  if (isset($_POST['level3id']) && isset($_POST['level3name'])) {
    $moreMetadata['level3name'] = $_POST['level3name'];
    $moreMetadata['level3id'] = $_POST['level3id'];
  }
  // sensitiveflag values should be 1/0 (1 == true)
  $moreMetadata['sensitiveflag'] = $_POST['sensitiveflag'];
  $senflag = $_POST['sensitiveflag'];
  if (($senflag != "1") && ($senflag != "0")) {
    sendResponse(STATUS_ERR, "sensitiveflag value not 1/0 [$senflag]!");
    exit(0);
  }
  if (isset($_POST['sensitiverat'])) {
    $moreMetadata['sensitiverat'] = $_POST['sensitiverat'];
  }
  if (isset($_POST['docauthor'])) {
    $moreMetadata['docauthor'] = $_POST['docauthor'];
  } else {
    $moreMetadata['docauthor'] = "";
  }
  if (isset($_POST['docsummary'])) {
    $moreMetadata['docsummary'] = $_POST['docsummary'];
  } else {
    $moreMetadata['docsummary'] = "";
  }
  if (isset($_POST['addressee'])) {
    $moreMetadata['addressee'] = $_POST['addressee'];
  } else {
    $moreMetadata['addressee'] = "";
  }

  $metadata['moreMetadata'] = $moreMetadata;

  /*
   * DEBUG: Write metadata struct to file
  $metadataStr = print_r($metadata, True);
  $fd = fopen("${TMPDIR}/${fileName}.metadata", "wb");
  if ($fd === FALSE) {
    sendResponse(STATUS_ERR, "Could not open ${TMPDIR}/${fileName}.metadata");
    exit(0);
  }
  fwrite($fd, $metadataStr);
  fclose($fd);
   */

  /*
  TODO:
  * If check-in failed because of datamart error, the request can be
    manually regenerated from the tempfiles.  Can probably run a script
    nightly (or on demand) to clean up the directory and re-checkin files
    and add missing datamart records.
  */


  /*
   * Check in new file
   *
   * Assume we're always running this from a front-end machine
   * and this machine always has a running broker on it
   * therefore we don't need the XMLRPC API ourselves, as we can call
   * the functions on the proxy object directly.
   */
  $drmProxy = new DrmProxy();

  // Open uploaded file
  $fp = fopen($tmpFileName, 'rb');
  if ($fp === FALSE) {
    sendResponse(STATUS_ERR, "Could not open ${tmpFileName}");
    exit(0);
  }
  $file = fread($fp, $size);
  fclose($fp);

  // Create temp files
  $projectid = $_POST['projectid'];
  $author = $_POST['author'];
  $prefix = "${projectid}_${author}_${time}";
  $fd = fopen("${TMPDIR}/${prefix}.file", "wb");
  if ($fd === FALSE) {
    sendResponse(STATUS_ERR, "Could not open ${TMPDIR}/${prefix}.file");
    exit(0);
  }
  fwrite($fd, $file);
  fclose($fd);
  $fd = fopen("${TMPDIR}/${prefix}.metadata", "wb");
  if ($fd === FALSE) {
    sendResponse(STATUS_ERR, "Could not open ${TMPDIR}/${prefix}.metadata");
    exit(0);
  }
  fwrite($fd, serialize($metadata));
  fclose($fd);

  $docid = "UNKNOWN";
  try {
    // Store file through local DMD app
    //
    // Note if this fails because DMD cannot connect to Stellent,
    // the script currently exits with no response, so the client gets back
    // an empty string instead of a JSON response
    //
    // I think this must be happening inside the SOAP library
    $docid = $drmProxy->putFile($file, $metadata, array());
  } catch (Exception $e) {
    sendResponse(STATUS_ERR, "putFile failed, exception " . $e->getMessage());
    exit(0);
  }

  try {
    // Record checkin status
    // Issue a POST to FILERECORDERHOST/filerecorder/putNewFileInfo.php
    // with jobid, docid, data
    $postURL = "https://$RECHOST/filerecorder/putNewFileInfo.php";
  
    // put "size" into metadata as "filesize", note this is a <string>
    $metadata['filesize'] = strval($size);

    $postdata = http_build_query(
      array(
        'jobid' => $jobid,
        'docid' => $docid,
        'data'  => serialize($metadata)
      )
    );

    $opts = array('http' =>
      array(
        'method' => 'POST',
        'header' => 'Content-type: application/x-www-form-urlencoded',
        'content' => $postdata
      )
    );

    $context = stream_context_create($opts);

    $logresult = file_get_contents($postURL, false, $context);
    if ($logresult === "0") {

      // Send response as a JSON object (code, message)
      sendResponse(STATUS_OK, $docid);

      // Remove temp files
      unlink("${TMPDIR}/${prefix}.file");
      unlink("${TMPDIR}/${prefix}.metadata");

    } else {

      sendResponse(STATUS_ERR, "Server response: $logresult");

    }

  } catch (Exception $e) {
    $errmessage = $e->getMessage();
    // If we couldn't record the status in a job record, delete the file!
    // (Don't leave any orphaned files)
    try {
      $drmProxy->deleteFile($docid);
    } catch (Exception $e) {
        $errmessage .= " Unable to delete $docid, " . $e->getMessage();
    }
    sendResponse(STATUS_ERR, "Submission error: $errmessage");
  }
}

/*
 *	Run main() in exception handler
 */
try {
  main();
} catch (Exception $e) {
  sendResponse(STATUS_ERR, "Unhandled: " . $e->getMessage());
}

?>
