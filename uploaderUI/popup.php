<?php exit(0); ?>
<html>
<head>

<script language="JavaScript">
function upload(appsysid, appdoctype, author, projectid, jobid) {
var appsysidArg = 'appsysid=' + encodeURI(appsysid);
var appdoctypeArg = '&appdoctype=' + encodeURI(appdoctype);
var authorArg = '&author=' + encodeURI(author);
var projectidArg = '&projectid=' + encodeURI(projectid);
var jobidArg = '&jobid=' + encodeURI(jobid);
var targetUrl = 'palsuploader.php?' + appsysidArg + appdoctypeArg + authorArg + projectidArg + jobidArg;
window.open(targetUrl,'window_name' + jobid,'width=800,height=600,resizable=no,scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no').focus();
// window.open(targetUrl,'window_name' + jobid,'width=800,height=600,resizable=yes,scrollbars=yes,toolbar=yes,location=no,directories=no,status=no,menubar=no,copyhistory=no').focus();
}
</Script>

</head>
<body>
<!-- Note that jobids should be generated dynamically -->
<!-- appsysid is either 01 (launched from doc tab)
     or 05 (launched from PFM module) -->
<a href="javascript:upload('01', 'Supporting', 'mhsu02', '33585', 'jobid1<?php echo time(); ?>')">Click here to add supporting documents to PALS TEST project</a>
<br/>
<br/>
<a href="javascript:upload('01', '', 'mhsu02', '33585', 'jobid1<?php echo time(); ?>')">Click here to add (no category) documents to PALS TEST project</a>
<br/>
<br/>
<a href="javascript:upload('01', 'Supporting', 'palsuser', '33585', 'jobid1<?php echo time(); ?>')">Click here to add supporting documents (bad user ID)</a>
<br/>
<br/>
<a href="javascript:upload('01', 'Supporting', 'mhsu02', '33585', 'jobid1<?php echo time(); ?>')">Click here to add supporting documents</a>
<br/>
<br/>
<a href="javascript:upload('05', 'Scoping', 'mhsu02', '33585', 'jobid2<?php echo time(); ?>')">Click here to add scoping documents</a>
<br/>
<br/>
<a href="javascript:upload('01', 'Analysis', 'mhsu02', '33585', 'jobid3<?php echo time(); ?>')">Click here to add analysis documents</a>
<br/>
<br/>
<a href="javascript:upload('05', 'Decision', 'mhsu02', '33585', 'jobid4<?php echo time(); ?>')">Click here to add decision documents</a>
<br/>
<br/>
<a href="javascript:upload('01', 'Appeal', 'mhsu02', 'projectid', 'jobid5<?php echo time(); ?>')">Click here to add appeal documents</a>
</body>
</html>
