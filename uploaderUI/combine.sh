#!/bin/bash -x

# Build combined and minified files for the Javascript and CSS used by
# the uploader page.
#
# Make sure file names are all up to date!
#
# This tool uses various minifiers (jsmin and yuicompressor) and is
# subject to change over time.

cat							\
../jquery/css/custom-theme/jquery-ui-1.8.18.custom.css	\
upload.css						\
> combined.css

java -jar ~/bin/yuicompressor.jar combined.css -o combined-min.css

cat							\
core/swfupload.js					\
js/swfupload.swfobject.js				\
js/swfupload.queue.js					\
../jquery/js/jquery-1.7.1.min.js				\
../jquery/js/jquery-ui-1.8.18.custom.min.js			\
js/handlers.js						\
js/uploadprogress.js					\
| ~/bin/jsmin > combined-min.js

