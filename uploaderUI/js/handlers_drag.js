// Javascript functions to be implemented:

// uploadStart
// uploadProgress
// uploadError
// uploadSuccess
// uploadComplete

// file_queued_handler
// file_queue_error_handler
// file_dialog_complete_handler

// queue_complete_handler

// swfUploadLoaded (fires when SWFUpload is loaded OK)

// The following are handlers for the SWFObject plugin:
// swfuploadPreLoad (fires when the minimum flash version is OK but before
//   the control is fully loaded -- could be used to display warning notice
//   in case loading takes too long)
// swfuploadLoadFailed (fires when the upload fails)

/*
 *	Job status codes
 */
JOB_OPEN = "O";
JOB_CLOSED = "C";

/*
 *	Generic service response codes
 */
RESP_OK = "0";

/*
 *	Blacklist of undesirable file extensions
 */
BLACKLIST = new Array("", "ad", "adp", "asp", "bas", "bat", "chm", "cmd", "com", "cpl", "crt", "exe", "hlp", "hta", "inf", "ins", "isp", "js", "jse", "lnk", "mdb", "mde", "msc", "msp", "msi", "mst", "pcd", "pif", "reg", "scr", "sct", "shb", "shs", "swf", "url", "vb", "vbe", "vbs", "vss", "vst", "vsw", "ws", "wsc", "wsf", "wsh");

/*
 *	Display page contents after SWF control is fully loaded
 *	This should be the first function to be invoked
 */
function swfUploadLoaded() {
	// SWFUpload control loaded OK
	$('#divLoadingContent').show();
	$('#divAlternateContent').hide();

	var swfu = $('#content').data('swfu');
	var opts = swfu.customSettings;
	opts.rejectList = new Array(); // display list for rejected files
	initWindow(swfu);
	disableUploadStart();
	disableFileSelect();
	disableUrlUploader();
	openJob(opts);	// also starts updateJob timer, enables controls
	//for some reason setting the text file after we return from this event (and actually display the flash button) doesn't work on IE
	//so our attempts to enable the button don't work correctly and the appearance stays disabled.  This forces the issue before we
	//are technically enabled (and if something fails it will likely stay showing enabled... only so much we can do)
	swfu.setButtonTextStyle('.myTxt { text-align: center; font-size: 18; font-family: sans-serif; font-weight: bold; color: #47571D; }');
}

function swfUploadLoadFailed() {
	// Flash does not meet minimum version requirements--this
	// error is actually kind of useless
	$('#divLoadingContent').hide();
	$('#divAlternateContent').show();
}

function validateFiletype(extension) {

  var ext = extension;
  if (ext.substring(0,1) == '.') {
    ext = ext.substring(1);
  }
  ext = ext.toLowerCase();

  for (var i=0; i<BLACKLIST.length; i++) {
    if (ext == BLACKLIST[i]) {
      return false;
    }
  }
  return true;
}

function htmlescape(str) {
    return str.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;");
}

/*
 *	Add an entry to swfu.customSettings.rejectList
 *	Each entry is a pair of [ filename, reason ] strings
 */
function makeRejectMsg(file, message) {
  var reject = new Object();
  reject.filename = file.name;
  reject.reason = message;
  swfu.customSettings.rejectList.push(reject);
}

function fileQueued(file) {
	// Perform custom validation here--if file fails validation then
	// boot it out of the queue
	// - Filetype blacklist
	// - Filename length restricted
	if (validateFiletype(file.type) == false) {
		makeRejectMsg(file, "Bad file type " + file.type);
		this.cancelUpload(file.id, false);
		return;
	}
	maxlen = this.customSettings.maxFilenameLength;
	if (file.name.length > maxlen) {
		makeRejectMsg(file, "Filename too long (max length " + maxlen + ")");
		this.cancelUpload(file.id, false);
		return;
	}
	var me = this;
	qAddFile(file, function() {
		me.cancelUpload(file.id, false);
		qRemoveFile(file.id);
		if (countPending(me) == 0) {
			disableUploadStart();
		}
	});
}

function fileQueueError(file, errorCode, message) {
	try {
		makeRejectMsg(file, message);
	} catch (ex) {
        this.debug(ex);
    }
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
	try {
		if (this.customSettings.rejectList.length > 0) {
			showRejects(this.customSettings.rejectList);
			this.customSettings.rejectList = new Array();
		}
		if (countPending(this) > 0) {
			enableUploadStart();
		} else {
			disableUploadStart();
		}
	} catch (ex)  {
        	this.debug(ex);
	}
}

function uploadStart(file) {
	// return true;
	qStartUpload(file.id);
	qSetProgress(file.id, 0);
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
	var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);
	qSetProgress(file.id, percent);
}

/*
 *	Upload is done, check response code to see if the server
 *	liked the upload or not
 */
function uploadSuccess(file, serverData) {
	var response = $.parseJSON(serverData);
	if (response['code'] == 0) {
		qSetComplete(file.id, response['message']);
	} else {
		qSetError(file.id, response['message']);
		addErrorToList(file, response['message']);
	}
}

function uploadError(file, errorCode, message) {
	var swfe = SWFUpload.UPLOAD_ERROR;
	var text;
	switch(errorCode)
	{
	case swfe.HTTP_ERROR:
		text = "Upload server response: " + message;
		qSetError(file.id, text);
		addErrorToList(file, text);
		break;
	case swfe.IO_ERROR:
		text = "Network connection interrupted";
		qSetError(file.id, text);
		addErrorToList(file, text);
		break;
	case swfe.FILE_CANCELLED:
		text = "File upload cancelled";
		qSetError(file.id, text);
		addErrorToList(file, text);
		break;
	case swfe.UPLOAD_STOPPED:
		text = "File upload stopped";
		qSetError(file.id, text);
		addErrorToList(file, text);
		break;
	default:
		text = Number(errorCode).toString() + " - " + message;
		qSetError(file.id, text);
		addErrorToList(file, text);
		break;
	}
}

// This event comes from the Queue Plugin
function queueComplete(numFilesUploaded) {
	closeJob(this.customSettings);
	disableQueueControls();
	enableExit();
	var swfu = $('#content').data('swfu');
	var stats = swfu.getStats();
	var errorlist = getErrorList();
	if (errorlist.length == 0) {
		modalDialogHTML('STATUS', 'Uploads complete!');
	} else {
		errorHTML = assembleErrorHTML("Uploads completed with errors", errorlist);
		modalDialogHTML('STATUS', errorHTML);
	}
}

/*
 * Open the new job
 * When the job is opened, enable file selection
 * and start the "updateJob" recurring job
 *
 * Error handling should consist of
 * - display error message
 * - retry if applicable
 */
function openJob(opts) {
	var postdata = { 'jobid' : opts.jobID };

	setWarningHTML("Opening new job, please wait...");

	$.post(opts.jobOpenURL, postdata,
		// Handle POST success
		function(data, stat, xhr) {
			if (data === RESP_OK) {
				/* Success */
				opts.jobStatus = JOB_OPEN;
				// Opened ok, user can start file selection
				enableFileSelect();
				enableUrlUploader();
				clearWarningHTML();
				startJobMonitor(opts);
			} else if (/^SQLCODE 00000/.test(data)) {
				// Duplicate job ID, should never happen
				clearWarningHTML();
				openJobFailed("Duplicate job ID?");
			} else {
				if (data == "") {
				    // Server was contacted but no response
				    // from application, possible timeout?
				    clearWarningHTML();
				    openJobFailed("No response from server");
				} else {
				    clearWarningHTML();
				    openJobFailed("Response: [" + data + "]");
				}
			}
		}
	)
	// Handle POST failure
	// xhr: jQuery XHR object
	// textStatus: null, "timeout", "error", "abort", "parsererror"
	// errorThrown: text of HTTP status, e.g. "Not Found",
	//     "Internal Server Error"
	.error(function(xhr, textStatus, errorThrown) {
		// xhr.status is 0 when state is UNSENT or OPENED
		// or when xhr's internal error flag is set
		// otherwise status is an HTTP response code
		if (xhr.status == 0) {
			// Handle by periodic retry
			openJobRetry(opts, "Could not connect to server");
		} else {
			// Generic error, do not retry
			var errMsg = "Error: " + textStatus;
			if (textStatus == "error") {
				errMsg = "HTTP response: " + errorThrown;
			}
			clearWarningHTML();
			openJobFailed(errMsg);
		}
	});
}

/*
 *	Start periodic job to check on network connection
 *	Only start monitor if job is open
 */
function startJobMonitor(opts) {
	if ((opts.jobUpdateTimer == 0) && (opts.jobStatus == JOB_OPEN)) {
		opts.updateJobRetries = 0;	// Reset retry limit
		opts.jobUpdateTimer = setInterval(
			function() { updateJob(opts); },
			30000 // attempt update every 30 sec
		);
	}
}

/*
 *	Stop periodic job monitor
 */
function stopJobMonitor(opts) {
	if (opts.jobUpdateTimer != 0) {
		clearInterval(opts.jobUpdateTimer);
	}
	opts.jobUpdateTimer = 0;
}

/*
 * If the job couldn't be opened, try again
 * Give up after 1 minute
 */
function openJobRetry(opts, reason) {
	var retryLimit = 12;

	setWarningHTML("Opening new job, retry " + (opts.openJobRetries + 1) + " of " + retryLimit);
	if (opts.openJobRetries < retryLimit) {
		opts.openJobRetries += 1;
		setTimeout(function() { openJob(opts); }, 5000);
	} else {
		clearWarningHTML();
		openJobFailed(reason);
	}
}

/*
 * Job couldn't be opened, quit trying
 */
function openJobFailed(reason) {
	modalDialogHTML("ERROR", "Could not start new job, please close window and try again later!<br/><br/><b>" + htmlescape(reason) + "</b>");
	disableQueueControls();
}

/*
 * Update timestamp on active job--tests net connectivity
 * and keeps session alive
 * This should get called periodically from an interval timer
 */
function updateJob(opts) {
	var postdata = { 'jobid' : opts.jobID };
	$.post(opts.jobUpdateURL, postdata,
		function(data, stat, xhr) {
			if (data === RESP_OK) {
				// Updated ok,
				// reset opts.updateFailureCount
				opts.updateJobRetries = 0;
				clearWarningHTML();
			} else {
				clearWarningHTML();
				if (data == "") {
				    // Server was contacted but no response
				    // from application, possible timeout?
				    updateJobFailed("No response from server");
				} else {
				    updateJobFailed("Response: [" + data + "]");
				}
			}
		}
	)
	.error(function(xhr, textStatus, errorThrown) {
		if (xhr.status == 0) {
			// Handle by periodic retry
			updateJobRetry(opts, "Could not connect to server");
		} else {
			// Generic error, do not retry
			var errMsg = "Error: " + textStatus;
			if (textStatus == "error") {
				errMsg = "HTTP response: " + errorThrown;
			}
			clearWarningHTML();
			updateJobFailed(errMsg);
		}
	});
}

/*
 * If the job couldn't be updated, try again
 * Give up after 1 minute
 */
function updateJobRetry(opts, reason) {
	var retryLimit = 5;

	setWarningHTML("Server connection lost, retry " + (opts.updateJobRetries + 1) + " of " + retryLimit);
	if (opts.updateJobRetries < retryLimit) {
		opts.updateJobRetries += 1;
		// job will retry based on interval timer
	} else {
		clearWarningHTML();
		updateJobFailed(opts, reason);
	}
}

/*
 * Too many update failures, cancel uploader session
 * Also cancel recurring updater job
 */
function updateJobFailed(opts, reason) {
	// cancel recurring update job
	stopJobMonitor(opts);

	// disable uploader window
	modalDialogHTML("ERROR", "Server connection lost, please close window and try again later!<br/><b>Reason: " + htmlescape(reason) + "</b>");
	disableQueueControls();
}

/*
 * Close the current job
 * disable all queue controls
 * and stop the "updateJob" recurring job
 */
function closeJob(opts) {
	// cancel job updates
	stopJobMonitor(opts);

	// disable controls
	disableQueueControls();

	if (opts.jobStatus == JOB_OPEN) {
		// close job
		var postdata = { 'jobid' : opts.jobID };
		$.post(opts.jobCloseURL, postdata,
			function(data) {
				// Ignore any errors,
				// let user close window
				disableQueueControls();
			}
		);
	}
	opts.jobStatus = JOB_CLOSED;
}

// display/clear warning messages
// (not full errors requiring a modal dialog)
function setWarningHTML(msg) {
	$("#warningDiv").html(msg);
	$("#warningDiv").show();
}

function clearWarningHTML() {
	$("#warningDiv").html("");
	$("#warningDiv").hide();
}

// ref to swfu is stored in #content.data
function enableFileSelect() {
	var swfu = $('#content').data('swfu');
	$('fileSelectBtn').css('background', '#000000');
	try {
		swfu.setButtonTextStyle('.myTxt { text-align: center; font-size: 18; font-family: sans-serif; font-weight: bold; color: #47571D; }');
	} catch (e) { }
	try {
		swfu.setButtonDisabled(false);	// enable SWF control
	} catch (e) {
	}
}

function disableFileSelect() {
	$('Filedata').css('background', '#E0E4C0');
	var swfu = $('#content').data('swfu');
	try {
		//swfu.setButtonTextStyle('.myTxt { text-align: center; font-size: 18; font-family: sans-serif; font-weight: bold; color: #B8C0A0; }');
	} catch (e) { }
	try {
		//swfu.setButtonDisabled(false);	// disabled SWF control
	} catch (e) {
	}
}

/*
 *	Functions to enable/disable UI buttons
 */

function enableUploadStart() {
	$("#startUploadBtn").click(
		function() {
			if (validateUploads()) {
 				disableFileSelect();
				disableUploadStart();
				disableExit();
				disableUrlUploader();
				beginUploads();
			}
		}
	);
	$("#startUploadBtn").button("enable");
}

function disableUploadStart() {
	$("startUploadBtn").unbind('click');
	$("startUploadBtn").button("disable");
}

function enableExit() {
	$("#exitBtn").click(handleExit);
	$("#exitBtn").button("enable");
}

function disableExit() {
	$("#exitBtn").unbind('click');
	$("#exitBtn").button("disable");
}

/*
 *	Enable URL uploader button
 */
function enableUrlUploader() {
	$("#urlUploaderBtn").click(showUrlUploader);
	$("#urlUploaderBtn").button("enable");
}

/*
 *	Disable URL uploader button
 */
function disableUrlUploader() {
	$("#urlUploaderBtn").unbind('click');
	$("#urlUploaderBtn").button("disable");
}

function disableQueueControls() {
	disableFileSelect();
	disableUploadStart();
	disableUrlUploader();
}

/*
 *	Functions to manage transitions between the original uploader
 *	and the URL uploader
 *
 *	These functions are not called at startup because they assume
 *	that the job has started, and the initialization of the window
 *	and the job are done independently
 */

/*
 *	Basic (multidocument) uploader will periodically ping the server
 *	and will close the job if the user kills the window
 */
function enterBasicUploaderState() {
	var swfu = $('#content').data('swfu');
	var opts = swfu.customSettings;
	// attach window exit event
	$(window).bind('beforeunload', doExitJobs);
	// if job is open, start job monitor
	startJobMonitor(opts);
}

/*
 *	Prepare the uploader to enter the URLUploader state.
 *
 *	Closing the URL uploader window should not close the job because
 *	this could cause the in-process job to fail
 *
 *	Note however that the filerecorder will close the job if it is open
 *	and has been idle for too long (30 minutes) so it is necessary for
 *	the URL uploader to keep pinging the server as long as the job is
 *	active.
 */
function exitBasicUploaderState() {
	// detach window exit event
	$(window).unbind('beforeunload');
	clearWarningHTML();
}

/*
 *	Perform any final validation checks prior to starting upload
 *
 *	If sensitiveflag is true then sensitiverat needs to have
 *	a value selected
 *	Document category (appdoctype) must be selected
 */
function validateUploads() {
	// js to inspect a radio button is kind of convoluted
	if ($('input:radio[name=sensitive]:checked').val() == "1") {
		if ($('#reason option:selected').val() == "") {
			// error, need to select a reason
			modalDialogHTML("ERROR", "Document(s) marked sensitive, please select a reason");
			return false;
		}
	} else {
		var swfu = $('#content').data('swfu');
		var appdoctype = swfu.customSettings.appdoctype;
		if (appdoctype == "") {
			// error, need to select a category
			modalDialogHTML("ERROR", "No category selected, please select a category");
			return false;
		}
	}
	return true;
}

/*
 *	Initialize upload params, start SWF uploader
 */
function beginUploads() {
	var swfu = $('#content').data('swfu');
	/* Add global POST params */
	swfu.addPostParam('docdate', $('#docDate').val());
	swfu.addPostParam('datelabel', $('#dateLabel').val());
	swfu.addPostParam('datecreated', $('#createDate').val());
	// To use YYYY-MM-DD format instead:
	// swfu.addPostParam('datecreated', $('#createDateAlt').val());
	if ($('#level2select').val() != "") {
	swfu.addPostParam('level2name', $('#level2select option:selected').text());
	swfu.addPostParam('level2id', $('#level2select').val());
	}
	if ($('#level3select').val() != "") {
	swfu.addPostParam('level3name', $('#level3select option:selected').text());
	swfu.addPostParam('level3id', $('#level3select').val());
        }
	var sensitiveFlag = ($('input:radio[name=sensitive]:checked').val());
	swfu.addPostParam('sensitiveflag', sensitiveFlag);
	if (sensitiveFlag == "1") {
	swfu.addPostParam('sensitiverat', $('#reason option:selected').val());
	}
	swfu.addPostParam('docauthor', $('#authorFrom').val());
	swfu.addPostParam('addressee', $('#addresseeTo').val());
	swfu.startUpload();
}

/*
 *	Start URL uploader in modal window
 */
function initUrlUploader() {
	var swfu = $('#content').data('swfu');
	var opts = swfu.customSettings;

        maxheight = $(window).height();
        maxwidth = $(window).width();

	var urluploader = $('div#divURLUploader').dialog({
		autoOpen: false,
		modal: true,
		closeOnEscape: true,
		resizable: false,
		height: maxheight,
		width: '100%',
		close: function(event, ui) {
			enterBasicUploaderState();
		}
	});

	urluploader.dialog('widget')	// returns .ui-dialog element
	.addClass('urluploader')	// hide titlebar via css
	.removeClass('ui-corner-all');	// remove rounded window corners

	$('#divURLUploader input[name=jobid]').val(opts.jobID);
	$('#divURLUploader input[name=appsysid]').val(opts.appSysID);
	$('#divURLUploader input[name=author]').val(opts.author);
	$('#divURLUploader input[name=projectid]').val(opts.projectID);
	$('#divURLUploader input[name=email]').val(opts.email);

	opts.urluploaderdialog = urluploader;

	$(window).resize(function() {
        	winheight = $(window).height();
		urluploader.dialog('option', 'height', winheight);
	});
}

/*
 *	Start URL uploader in modal window
 */
function showUrlUploader() {
	var swfu = $('#content').data('swfu');
	var opts = swfu.customSettings;

	exitBasicUploaderState();
	opts.urluploaderdialog.dialog('open');
}

  /*
   * Find appropriate list of lvl2 groups given a list of categories (lvl1)
   * Return empty list if category not found
   */
  function getlvl2(appdoctype, list) {
    lvl2list = [];
    for (var n = 0; n < list.length; n++) {
      if (list[n].label == appdoctype) {
        lvl2list = list[n].contents;
        break;
      }
    }
    return lvl2list;
  }

  /*
   *  Given a list of categories, populate the category select drop-down
   *
   *  Alphabetize list
   *
   *  Update title banner and swfu parameters if the category changes,
   *  as appdoctype was previously a required field and these values used
   *  to be invariant as soon as the window was opened
   *
   *  Each list item has a "contents" property consisting of a list of
   *  groups, used to populate the level2 select boxes (and those contain
   *  lists of subgroups, as well)
   */
  function setlevel1select(list) {
    var id = "#level1select";
    if (list == null) { list = []; }
    list.sort(function(a,b) { 
      return a.order > b.order;
    });
    $(id).html("");
    if (list.length == 0) {
      $("<option value=''>No category choices</option>").appendTo(id);
      $(id).prop('disabled', 'disabled');
    } else {
      $("<option value='' selected='true'>Select category</option>").appendTo(id);
      $(id).prop('disabled', '');
      addoptions(id, list);
    }
    var swfu = $('#content').data('swfu');
    $(id).change(function() {
      var node = $(id + " option:selected").first();
      $("#title").text(node.text());	// change title banner
      if (node.val() != "") {
          swfu.customSettings.appdoctype = node.text(); // use name, not value
      } else {
          swfu.customSettings.appdoctype = "";
      }
      var postparams = swfu.settings.post_params;
      postparams.appdoctype = swfu.customSettings.appdoctype;
      swfu.setPostParams(postparams);
      var contents = node.data('contents');
      setlevel2select(contents);
    });
    $("#title").text("Select category");	// set default title banner
    setlevel2select([]);
  }

  /*
   * Categories aren't required to have distinct contids or labels, so
   * these aren't good choices for select values.
   *
   * Categories are supposed to have distinct contids and labels though.
   *
   * There has never been any guarantee of uniqueness for "order".
   */
  function setlevel2select(list) {
    var id = "#level2select";
    if (list == null) { list = []; }
    list.sort(function(a,b) { 
      return a.order > b.order;
    });
    $(id).html("");
    if (list.length == 0) {
      $("<option value=''>No group choices</option>").appendTo(id);
      $(id).prop('disabled', 'disabled');
    } else {
      $("<option value='' selected='true'>Select group</option>").appendTo(id);
      $(id).prop('disabled', '');
      addoptions(id, list);
    }
    $(id).change(function() {
      var contents = $(id + " option:selected").first().data('contents');
      setlevel3select(contents);
    });
    setlevel3select([]);
  }

  function setlevel3select(list) {
    var id = "#level3select";
    if (list == null) { list = []; }
    list.sort(function(a,b) { 
      return a.order > b.order;
    });
    $(id).html("");
    if (list.length == 0) {
      $("<option value=''>No subgroup choices</option>").appendTo(id);
      $(id).prop('disabled', 'disabled');
    } else {
      $("<option value='' selected='true'>Select subgroup</option>").appendTo(id);
      $(id).prop('disabled', '');
      addoptions(id, list);
    }
  }

  function addoptions(id, list) {
    for (var n = 0; n < list.length; n++) {
      var disp = list[n].label;
      var contid = list[n].contid;
      var contents = list[n].contents;
      $("<option value='" + contid + "'>" + disp + "</option>").appendTo(id);
      $(id).children(":last").data('contents', contents);
    }
  }

  /*
   * Prune the container tree so that documents cannot be uploaded to specific
   * subcategories from the Uploader GUI
   *
   * Subcategories are specified as paths of labels.  This is a terrible
   * design but now we're stuck with it.  Note that labels are neither
   * enforcably unique nor required.
   *
   * If, for some strange reason, we discover multiple routes to the same
   * undesirable path of labels, we're going to have to remove all of them.
   *
   * Nodes in the container tree contain the following attributes:
   *     order    - int, possibly null
   *     label    - string, possibly null
   *     contid   - int, possibly null
   *     contents - array, possibly empty
   *
   * For Appeals: Post-Decision / Appeals / Appeal responses
   * For CARA:    TBD
   */

function restrictContainers(list) {
 pruneContainer(list, ['Analysis', 'Objections' ]);
pruneContainer(list, ['Post-Decision', 'Appeals']);
pruneContainer(list, ['Supporting', 'Public Involvement']);
for (var j=0; j<list.length; j++) {
                 var item = list[j];
                 if (item.label == 'Litigation')
                 list.splice(j, 1);
                 }
}


  function pruneContainer(list, targetpath) {
    switch (targetpath.length) {
      case 0:  /* do nothing */
               break;
      case 1:  /* prune from end of list */
               for (var i=list.length-1; i>=0; i--) {
                 var item = list[i];
                 if (item.label == targetpath[0]) {
                   list.splice(i,1);
                 }
               }
               break;
      default: /* recursively prune sublist */
               for (var j=0; j<list.length; j++) {
                 var item = list[j];
                 if (item.label == targetpath[0]) {
                   pruneContainer(item.contents, targetpath.slice(1));
                 }
               }
               break;
    }
  }

function initWindow(swfu) {
	// beforeunload is not officially supported but
	// exists in both IE and FF
	// Don't try to block window exit, but try to clean up first
	$(window).bind('beforeunload', doExitJobs);

	$(".btn").button();	// style div buttons as jQueryUI buttons

	// swfu dynamic button functions always fire meaningless exceptions
	// and must be caught
	try {
	//	swfu.setButtonDimensions(112,47);
	} catch (e) { }

	try {
	//	swfu.setButtonText('<span class="myTxt">Browse</span>');
	} catch (e) { }

	try {
	//	swfu.setButtonTextPadding(2, 8);
	} catch (e) { }

	try {
	//	swfu.setButtonTextStyle('.myTxt { text-align: center; font-size: 18; font-family: sans-serif; font-weight: bold; color: #47571D; }');
	} catch (e) { }

	//qInitLegend();
	//qInitMetadata();

	var choices = $('#content').data('containers');
	restrictContainers(choices['contents']);

	var appdoctype = swfu.customSettings.appdoctype;
	if (appdoctype == "") {
		// no requested category, let user pick one
		setlevel1select(choices['contents']);
	} else {
		// use requested category
		$("#targetCategory").hide();
		$("#title").text(appdoctype);	// set title banner
		var lvl2node = getlvl2(swfu.customSettings.appdoctype , choices['contents']);
		setlevel2select(lvl2node);
	}

	// Set change event handler for sensitivity radio buttons
	$("input[name='sensitive']").change(
	  function() {
	    if ($("input[name='sensitive']:checked").val() == "1") {
	      $("#reason").prop('disabled', '');
	    } else {
	      $("#reason").prop('disabled', 'disabled');
	      $("#reason option[value='']").prop('selected', 'selected');
	    }
	    $(this).blur(); // Drop focus from radio button
	  }
	);

	$("input[name='sensitive']:checked").change();	// fire to set default

	enableExit();
	initUrlUploader();
	enableUrlUploader();
}

function handleExit() {
	var swfu = $('#content').data('swfu');
	if (swfu.customSettings.exiting) {
		// already started exit proc, this is a duplicated event
		return;
	}

	swfu.customSettings.exitButtonClicked = true;
	doExitJobs();
}

/*
 *	Window is exiting, perform any cleanup tasks
 *
 *	Cancel anything in the upload queue,
 *	close the current job and halt the monitor if necessary,
 *	send a request to the PALS notification URL to indicate job-complete
 */
function doExitJobs() {
	var swfu = $('#content').data('swfu');
	var opts = swfu.customSettings;
	if (opts.exiting) {
		// already started exit proc, this is a duplicated event
		return;
	}

	opts.exiting = true;
	swfu.cancelQueue();
	closeJob(opts);
	$("#hiddenExitForm").submit();
	if (opts.exitButtonClicked) {
		window.close();
	}
}

/*
 *	Modal dialog, assume message text is HTML
 *	(can also pass in a jQuery object as message)
 */
function modalDialogHTML(mytitle, msg) {
	$("<div><span class='dialogTitle'>" + mytitle + "</span><br/></div>")
	.find("br").after(msg).end()
	.css('text-align', 'center')
	.dialog({
		modal: true,
		closeOnEscape: false,
		resizable: false,
		buttons: {
			'OK': function() {
				$(this).dialog('destroy');
			}
		}
	})
	.dialog('widget')		// returns .ui-dialog element
	.addClass('modalDialog');	// hide titlebar via css

	// center the exit button
	$('.modalDialog .ui-button').position({
		my: 'top center',
		at: 'top center',
		of: $('.modalDialog .ui-dialog-buttonpane')
	});
}

/*
 * Count number of queued or in-progress files
 *
 * Other statuses that don't count are:
 *   ERROR
 *   COMPLETE
 *   CANCELLED
 */
function countPending(swfu) {
  var count=0, i=0, file=null;

  do {
      file = swfu.getFile(i++);
      if (file != null) {
        if ((file.filestatus == SWFUpload.FILE_STATUS.QUEUED) ||
	    (file.filestatus == SWFUpload.FILE_STATUS.IN_PROGRESS)) {
                count += 1;
        }
      }
  } while (file != null);
  return count;
}

/*
 *	Returns jQuery display HTML for file errors
 */
function assembleErrorHTML(title, errorList) {
	var listHead = '<tr><td>Reason</td><td>Filename</td></tr>';
	var listItems = '';
	var sortList = errorList.sort(function(a, b) {
		return (a.filename > b.filename);
	});
	$.each(sortList, function(index, value) {
		listItems += '<tr><td>' +
			htmlescape(value.reason) +
			'</td><td>' +
			htmlescape(value.filename) +
			'</td></tr>';
	});
	// listItems = "<ul>" + listItems + "</ul>";
	var table =	"<table>" +
			"<thead>" + listHead + "</thead>" +
			"<tbody>" + listItems + "</tbody>"; +
			"</table>";

	var msg = htmlescape(title) + ":<br/>";

	// var bodyHTML = "<div>" + msg + table + "</div>";
	var bodyHTML = $("<div>" + msg + table + "</div>")
		.find("tbody tr:odd")
			.css('background', '#fbf8e0')
		.end()
		.find("tbody tr:even")
			.css('background', '#e0e0c3')
		.end()
		.find("table")
			.css('border', '1')
			.css('border-style', 'solid')
			.css('border-color', '#47571d')
		.end();
	return bodyHTML;
}

/*
 *	Show list of rejected files in Modal dialog
 */
function showRejects(rejectList) {
	bodyHTML = assembleErrorHTML("The following items could not be queued:", rejectList);
	modalDialogHTML('FILES REJECTED', bodyHTML);
}

function getErrorList() {
	var swfu = $('#content').data('swfu');
	if (("errorlist" in swfu) === false) {
		swfu.errorlist = new Array();
	}
	var errorlist = swfu.errorlist;
	return errorlist;
}

function addErrorToList(file, message) {
	var errorlist = getErrorList();
	var name = file.name;
	if (name.length > 20) {
		name = name.substr(0,17) + "...";
	}
	var error = new Object();
	error.filename = name;
	error.reason = message;
	errorlist.push(error);
}

