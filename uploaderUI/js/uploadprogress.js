/*
	Display upload progress in a list
	Each item contains
	* filename
	* progressbar
	* status icon

	The <li> tag has an ID that matches the fileID from swfupload

 *	<ul>
 *	  <li id="fileID">
 *	    <div class="qContainer> // start relative positioning
 *
 *            // use absolute positioning for icon (left) and cancelbtn (right)
 *	      <div id="statusID" class="statusicon ui-icon"></div>
 *	      <div id="cancelID" class="cancelicon ui-icon"></div>
 *
 *            // use absolute positioning for progress bar
 *	      <div id="pbarID">
 *	        <span class="pblabel">fname</span>
 *	      </div>
 *
 *	    </div> // qContainer
 *	  </li>
 *	</ul>
 */

/*
 *	Icon definitions
 */
var iconQueued = "ui-icon-document";
var iconProgress = "ui-icon-arrowthick-1-e";
var iconError = "ui-icon-notice";
var iconDone = "ui-icon-check";

var allStatusIcons = iconQueued + " " + iconProgress + " "
	+ iconError + " " + iconDone;

var iconCancel = "ui-icon-circle-close";

/*
 *	Create a progress item in the display list at the given ID
 *
 *	Take a callback function for use with the cancel button
 *
 *	Store a copy of the SWFUpload "file" object in the <li> node
 *	Note that the filestatus on this copy will be out of date if
 *	you fetch it later.
 */
function qAddFile(file, cancelfn) {
	var id = "file" + file.id;
	var statusbox = "status" + file.id;
	var cancelbox = "cancel" + file.id;
	var pbar = "pbar" + file.id;
	var fname = file.name;

        $('<li id="' + id + '">'
		+ '<div class="qContainer">'
		+ '<div id="' + statusbox + '" class="statusicon ui-icon">'
		+ '</div>'
		+ '<div id="' + cancelbox + '" class="cancelicon ui-icon">'
		+ '</div>'
		+ '<div id="' + pbar + '">'
		  + '<span class="pblabel">' + htmlescape(fname) + '</span>'
		+ '</div>'
		+ '</div>'
		+ '</li>')
		.appendTo("#uploadQueue");
        $("#" + id).data('file', file);
	qSetStatus(file.id, iconQueued, 'File queued');
        $("#" + cancelbox).addClass(iconCancel).click(cancelfn);
        $("#" + pbar).progressbar({ value: 0 });

	qUpdateBkgd();
}

function qRemoveFile(fileID) {
	var id = "file" + fileID;
	var pbar = "pbar" + fileID;
        $("#" + pbar).progressbar("destroy");
        $("#" + id).remove();
	qUpdateBkgd();
}

/*
 *	Assign background colors for queue entries
 */
function qUpdateBkgd() {
	$("#uploadQueue li:odd").css("background", "#fbf8e0");
	$("#uploadQueue li:even").css("background", "#e0e0c3");
}

function qEnableCancel(fileID) {
        $("#cancel" + fileID).show();
}

function qDisableCancel(fileID) {
        $("#cancel" + fileID).hide();
}

function qStartUpload(fileID) {
	qSetStatus(fileID, iconProgress, "Upload in progress");
}

function qSetComplete(fileID, docID) {
	qDisableCancel(fileID);
	qSetStatus(fileID, iconDone, docID);
}

function qSetError(fileID, message) {
	qDisableCancel(fileID);
	if (message != null) {
		qSetStatus(fileID, iconError,
			"Upload failed!  " + message);
	} else {
		qSetStatus(fileID, iconError,
			"Upload failed!");
	}
}

function qSetCancelled(fileID) {
	qDisableCancel(fileID);
	qSetStatus(fileID, iconError, "Upload cancelled");
}

function qSetProgress(fileID, percentage) {
	var pbar = "pbar" + fileID;
        $("#" + pbar).progressbar('value', percentage);
}

function qSetStatus(fileID, newIcon, message) {
	var statusAnchor = "#status" + fileID;
	$(statusAnchor).removeClass(allStatusIcons);
	$(statusAnchor).addClass(newIcon);

	if (newIcon == iconProgress) {
		$(statusAnchor).qtip({
		   content: "Upload in progress",
		   show: 'mouseover',
		   hide: 'mouseout'
		})
	}
	if (newIcon == iconError) {
		$(statusAnchor).qtip({
		   content: htmlescape(message),
		   show: 'mouseover',
		   hide: 'mouseout'
		})
	}
	if (newIcon == iconDone) {
		$(statusAnchor).qtip({
		   content: "Upload successful, docid = " + message,
		   show: 'mouseover',
		   hide: 'mouseout'
		})
	}
}

function qInitLegend() {
	$("#legend").addClass("ui-widget");
	$("#legendHeader").addClass("ui-widget-header");
	$("#legendContent").addClass("ui-widget-content");

	$("#iconQueued").addClass("ui-icon " + iconQueued);
	$("#iconProgress").addClass("ui-icon " + iconProgress);
	$("#iconError").addClass("ui-icon " + iconError);
	$("#iconDone").addClass("ui-icon " + iconDone);
	$("#iconCancel").addClass("ui-icon " + iconCancel);

	$("#legendHeader").css("color", "#f0f0f0");
	$("#legendHeader").css("background", "#47571d");
	$("#legendContent div:odd").css("background", "#FFFFFF");
}

function qInitMetadata() {
        $("#docDate").datepicker();
        $("#docDate").datepicker("setDate", "+0d");
        $("#docDate").css("width", "8em");

        $("#createDate").datepicker({
		altField: "#createDateAlt",
		altFormat: "yy-mm-dd"
	});
        $("#createDate").css("width", "8em");

	$("#applyMetadata > div:even").css("float", "left");
	$("#applyMetadata > div:odd").css("float", "right");
}

