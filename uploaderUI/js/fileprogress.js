/*
	A simple class for displaying file information and progress
	Note: This is a demonstration only and not part of SWFUpload.
	Note: Some have had problems adapting this class in IE7. It may not be suitable for your application.
*/

// Constructor
// file is a SWFUpload file object
// targetID is the HTML element id attribute that the FileProgress HTML structure will be added to.
// Instantiating a new FileProgress object with an existing file will reuse/update the existing DOM elements
function FileProgress(file, targetID) {
	this.fileProgressID = file.id;

	this.opacity = 100;
	this.height = 0;
	
		// this has to match the order of the elements
		// and it has to be set unconditionally
		this.PBAR    = 0;
		this.PFSIZE  = 1;
		this.PSTATUS = 2;
		this.PTEXT   = 3;
		this.PCANCEL = 4;

	this.fileProgressWrapper = document.getElementById(this.fileProgressID);
	if (!this.fileProgressWrapper) {
		this.fileProgressWrapper = document.createElement("div");
		this.fileProgressWrapper.className = "progressWrapper";
		this.fileProgressWrapper.id = this.fileProgressID;

		this.fileProgressElement = document.createElement("div");
		this.fileProgressElement.className = "progressContainer";

		var progressCancel = document.createElement("a");
		progressCancel.className = "progressCancel";
		progressCancel.href = "#";
		progressCancel.style.visibility = "hidden";
		progressCancel.appendChild(document.createTextNode("Cancel"));

		var progressFileSize = document.createElement("div");
		progressFileSize.className = "progressFileSize";
		var label = Math.ceil((file.size / 1024) * 10) / 10 + 'k';
		progressFileSize.appendChild(document.createTextNode(label));

		var progressText = document.createElement("div");
		progressText.className = "progressName";
		progressText.appendChild(document.createTextNode(file.name));

		var progressBar = document.createElement("div");
		progressBar.className = "progressBarInProgress";
		/* 
		If you use an &nbsp; here, the progress meter will be the
		height of a space character, if the height property is not
		specified in the CSS.
		progressBar.innerHTML = "&nbsp;";
		*/

		var progressBarOuter = document.createElement("div");
		progressBarOuter.className = "progressBarOuter";
		progressBarOuter.appendChild(progressBar);

		var progressStatus = document.createElement("div");
		progressStatus.className = "progressBarStatus";
		progressStatus.innerHTML = "&nbsp;";

		// this.fileProgressElement.appendChild(progressBar);
		this.fileProgressElement.appendChild(progressBarOuter);
		this.fileProgressElement.appendChild(progressFileSize);
		this.fileProgressElement.appendChild(progressStatus);
		this.fileProgressElement.appendChild(progressText);
		this.fileProgressElement.appendChild(progressCancel);

/*
 *  <div class="progressWrapper" id=file.id>
 *    <div class="progressContainer">	(aka fileProgressElement)
 *      <a class="progressCancel" href="#" style="visibility: hidden"></a>
 *      <div class="progressName">filename</div>
 *      <div class="progressBarStatus"/>&nbsp;</div>
 *      <div class="progressBarInProgress"/>
 *    </div>
 *  </div>
 */

/*
 * What we want is:
 * Project - filename size <span>progress bar</span> <a href="">cancel</a>
 */

		this.fileProgressWrapper.appendChild(this.fileProgressElement);

		document.getElementById(targetID).appendChild(this.fileProgressWrapper);
	} else {
		this.fileProgressElement = this.fileProgressWrapper.firstChild;
		this.reset();
	}

	this.height = this.fileProgressWrapper.offsetHeight;
	this.setTimer(null);

}

FileProgress.prototype.setTimer = function (timer) {
	this.fileProgressElement["FP_TIMER"] = timer;
};
FileProgress.prototype.getTimer = function (timer) {
	return this.fileProgressElement["FP_TIMER"] || null;
};

FileProgress.prototype.getProgressBar = function () {
	// return this.fileProgressElement.childNodes[this.PBAR];
	var outerBar = this.fileProgressElement.childNodes[this.PBAR];
	return outerBar.firstChild;
}

FileProgress.prototype.reset = function () {
	this.fileProgressElement.className = "progressContainer";

	this.fileProgressElement.childNodes[this.PSTATUS].innerHTML = "&nbsp;";
	this.fileProgressElement.childNodes[this.PSTATUS].className = "progressBarStatus";
	
	var progressBar = this.getProgressBar();
	progressBar.className = "progressBarInProgress";
	progressBar.style.width = "0%";
	
	this.appear();	
};

FileProgress.prototype.setProgress = function (percentage) {
	this.fileProgressElement.className = "progressContainer blue";
	var progressBar = this.getProgressBar();
	progressBar.className = "progressBarInProgress";
	progressBar.style.width = percentage + "%";
	this.appear();	
};
FileProgress.prototype.setComplete = function () {
	this.fileProgressElement.className = "progressContainer green";
	var progressBar = this.getProgressBar();
	progressBar.className = "progressBarComplete";
	progressBar.style.width = "";

/*
	var oSelf = this;
	this.setTimer(setTimeout(function () {
		oSelf.disappear();
	}, 10000));
*/
};
FileProgress.prototype.setError = function () {
	this.fileProgressElement.className = "progressContainer red";
	var progressBar = this.getProgressBar();
	progressBar.className = "progressBarError";
	progressBar.style.width = "";

/*
	var oSelf = this;
	this.setTimer(setTimeout(function () {
		oSelf.disappear();
	}, 5000));
*/
};
FileProgress.prototype.setCancelled = function () {
	this.fileProgressElement.className = "progressContainer";
	var progressBar = this.getProgressBar();
	progressBar.className = "progressBarError";
	progressBar.style.width = "";
	/* This has to be done manually because there is no reliable callback
	   when the cancel link is pressed--the uploadError callback is not
	   called if the file is cancelled before being uploaded */
	this.setStatus("Cancelled");

/*
	var oSelf = this;
	this.setTimer(setTimeout(function () {
		oSelf.disappear();
	}, 2000));
*/
};

FileProgress.prototype.setStatus = function (status) {
	this.fileProgressElement.childNodes[this.PSTATUS].innerHTML = status;
};

// Show/Hide the cancel button
FileProgress.prototype.toggleCancel = function (show, swfUploadInstance) {
	this.fileProgressElement.childNodes[this.PCANCEL].style.visibility = show ? "visible" : "hidden";
	if (swfUploadInstance) {
		var fileID = this.fileProgressID;
		var oSelf = this;
		this.fileProgressElement.childNodes[this.PCANCEL].onclick = function () {
			swfUploadInstance.cancelUpload(fileID);
			oSelf.setCancelled();
			// Not clear why the orig demo doesn't need this
			this.style.visibility = "hidden";
			return false;
		};
	}
};

FileProgress.prototype.appear = function () {
	if (this.getTimer() !== null) {
		clearTimeout(this.getTimer());
		this.setTimer(null);
	}
};

// Fades out and clips away the FileProgress box.
FileProgress.prototype.disappear = function () {
};
