/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
(function() {
var files;
var filesdata = new Array(10);
var count;
var count_ready;

/*
 *	Blacklist of undesirable file extensions
 */
BLACKLIST = new Array("", "ad", "adp", "asp", "bas", "bat", "chm", "cmd", "com", "cpl", "crt", "exe", "hlp", "hta", "inf", "ins", "isp", "js", "jse", "lnk", "mdb", "mde", "msc", "msp", "msi", "mst", "pcd", "pif", "reg", "scr", "sct", "shb", "shs", "swf", "url", "vb", "vbe", "vbs", "vss", "vst", "vsw", "ws", "wsc", "wsf", "wsh");

var ERROR_STATUS = "Error : Upload Failed";
var UPLOADING_STATUS = "Uploading";
var UPLOADED_STATUS = "Uploaded";
var ERROR_MESSAGE = "Error!";
var ONE_OR_MORE_FILES_FAILED = "One or more files failed to upload. Please close this window " +
		                       "and try again, or report a ticket.";
var SUCCESS_MESSAGE = "Success!";
var ALL_FILES_UPLOADED = "Queued files have uploaded successfully! Please close this window.";
var ERROR_SYMBOL = "ui-icon-notice";
var UPLOADING_SYMBOL = "ui-icon-arrowthick-1-e";
var UPLOADED_SYMBOL = "ui-icon-check";
var UI_ICON = "ui-icon";
var STATUS_ICON = "statusicon";

	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}

    function resetClassListFor(element){
        var newClasses = "";
        var classes = element.className.split(" ");
        for (var i = 0; i < classes.length; i++){
            if (classes[i] === UI_ICON || classes[i] === STATUS_ICON){
                newClasses += classes[i] + " ";
            }
        }
        element.className = newClasses;
    }
  
    function addClassTo(element, cssClass){
        element.className = element.className + " " + cssClass;
    }


	// output information
	function Output(msg) {
		var m = $id("messages");
		m.innerHTML = msg + m.innerHTML;
	}
 
	// output information
	function OutputAlert(title,msg) {
 
		var m = $id("messages");
        var str_msg="";
        if(msg!=null && msg!=""){
             var str_msg="<div class=\"ui-widget\">" +
                    "<div class=\"ui-state-highlight ui-corner-all\" style=\"padding: 0 .7em;\">" + 
                    "<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>" +
                    "<strong>" + title + ":</strong> " + msg + "</p></div></div>";
        }
		m.innerHTML = str_msg;
	}

	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "hover" : "");     
	}

 // Enable Uploadbutton
	function enableUploadbtn() {
     var btn_upload = $id("StartUpload");
     btn_upload.innerHTML = "<label for=\"uploadFiledata\" id=\"startUploadBtn_js\" class=\"custom-file-upload btn  ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\"><i class=\"fa fa-cloud-upload\" ></i> <strong>Upload Files</strong></label>";
     submitbtn = $id("startUploadBtn_js");
     submitbtn.addEventListener("click", FormSubmitHandler, false);
	}
 
  // Disable Uploadbutton
	function disableUploadbtn() {
     var btn_upload = $id("StartUpload");
     btn_upload.innerHTML = "<label for=\"uploadFiledata\" id=\"startUploadBtn_js\" class=\"custom-file-upload ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-disabled ui-state-disabled\"><i class=\"fa fa-cloud-upload\" ></i> <strong>Upload Files</strong></label>";
	}
  
  // Disable Browsebutton
	function disableBrowsebtn() {
     var btn_browse = $id("Filebrowse");
     btn_browse.innerHTML = "<label for=\"Filedata\" id=\"lblFiledata\" class=\"custom-file-upload ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-disabled ui-state-disabled \"><i class=\"fa fa-cloud-upload\" ></i> <strong>Browse</strong></label>";
     	var queueArea=$id("queueArea");
        queueArea.removeEventListener("drop", FileSelectHandler, false);
        queueArea.addEventListener("drop", dummyFileSelectHandler, false);
	}

function dummyFileSelectHandler(e) {
    // cancel event and hover styling
    FileDragHover(e);
    OutputAlert("Alert","Duplicate JobID, Please reload the page again!");
	}

	// file selection
	function FileSelectHandler(e) {
    OutputAlert("","");
    if(count==null) {
    	count = 0;
    }   
    // cancel event and hover styling
    FileDragHover(e);
    console.log("File Queued" );
    // fetch FileList object
    var files = e.target.files || e.dataTransfer.files; 
		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
   	  filesdata[count] = new Array(5);	
			filesdata[count]["name"]=f.name;
			filesdata[count]["type"]=f.type;
			filesdata[count]["size"]=(f.size/(1024));
			filesdata[count]["status"]="File Queued";
			filesdata[count]["file"]=f;
			// Perform custom validation here--if file fails validation then
    	// boot it out of the queue
    	// - Filetype blacklist
    	// - Filename length restricted
    	maxlen = 255;
      file_size_limit = 148480; //-file size limit in KB;  145MB = 148480KB
     var fext=getExtension(filesdata[count]["name"]);
    	if (validateFiletype(fext) == false) {
         filesdata[count]["status"]="Error: Bad file type " + fext;
    	}else if (filesdata[count]["name"].length > maxlen) {
        filesdata[count]["status"]="Error: Filename too long (max length " + maxlen + ")";
    	}else if (filesdata[count]["size"] > file_size_limit) {
        filesdata[count]["status"]="ERROR: File too large (max limit " + file_size_limit + " KB)";
    	}

      count++;      
      ParseFile();

		}
	}
 
 
 function getExtension(fname) {
  var pos = fname.lastIndexOf(".");
  var strlen = fname.length;
  if (pos != -1 && strlen != pos + 1) {
    var ext = fname.split(".");
    var len = ext.length;
    var extension = ext[len - 1].toLowerCase();
  } else {
    extension = "no extension";
  }
  return extension;
}
 
 
 function validateFiletype(extension) {
  var ext = extension;
  if(ext=="no extension"){
    return false;
  }
  if (ext.substring(0,1) == '.') {
    ext = ext.substring(1);
  }
  ext = ext.toLowerCase();

  for (var i=0; i<BLACKLIST.length; i++) {
    if (ext == BLACKLIST[i]) {
      return false;
    }
  }
  return true;
 }
 
 // remove FileData
    function removeFileData(e) {
        console.log("remove filedata...");
        var ind=e.target.getAttribute("value");
        OutputAlert("","");
        if(count>0){
            count --;
            parseOneFile(ind, "delete");
        }   
    }
 
 // file submit Handler
  function FormSubmitHandler(e) {
    OutputAlert("","");
    if ($id("appdoctype").value!="" && $id("appdoctype").value!=="Select category") {
         if(count !=null && count>=1){
             disableBrowsebtn();
             disableUploadbtn();
              // process all File objects
              uploadFiles();
          }else{
            OutputAlert("Alert","No files selected, please select/drop at least one file!");
          }
      }else{
     OutputAlert("ERROR","No category selected, please select a category");
     //filesdata[ind]["status"]= "Select Target Category!";
     }
  }
 
 // file selection
	function Selectlvl1Handler(e) {
     OutputAlert("","");
   	$id("appdoctype").value=(e.target.options[e.target.selectedIndex].text);
    console.log($id("level2select").disabled);
    console.log($id("docdate").value);
	}

// updates 1 file, and only updates its status and the display of the
// cancel button.
    function parseOneFile(ind){
        if (arguments[1] === "delete"){
            var parent = $id("uploadQueue");
            var child = $id("fileSWFUpload_0_" + ind);
            var pblabel = $id("pbarSWFUpload_0_" + ind);
            var filename = pblabel.childNodes[0].childNodes[0].data.trim();
            
            for (var i = 0; i < filesdata.length; i++){
                var file = filesdata[i];
                
                if (file.name.trim() === filename.trim()){
                    filesdata.splice(i, 1);
                    break;
                }
            }
            
            parent.removeChild(child);
            return;
        }
	var status = filesdata[ind]["status"];
	var id = "smallSWFUpload_0_" + ind;
	var elmnt = document.getElementById(id);
	var text = elmnt.innerHTML;
	text = text.replace(/(\|)(.*)/, "$1 " + status);
	elmnt.innerHTML = text;
	var small = document.getElementById("smallSWFUpload_0_" + ind);
    var statusSymbol = document.getElementById("statusSWFUpload_0_" + ind);
	if (status.indexOf(UPLOADED_STATUS) !== -1){
	    //remove cancel symbol
	    var cancel = document.getElementById("cancelSWFUpload_0_" + ind);
	    cancel.setAttribute("style", "display:none;");
            small.setAttribute("style", "color:#128840;font-weight:bold;");
            resetClassListFor(statusSymbol);
            addClassTo(statusSymbol, UPLOADED_SYMBOL);
	} else if (status.indexOf(ERROR_STATUS) !== -1){
	    var cancel = document.getElementById("cancelSWFUpload_0_" + ind);
	    cancel.setAttribute("style", "display:none;");
			small.setAttribute("style", "color:#e62226;");
                        resetClassListFor(statusSymbol);
                        addClassTo(statusSymbol, ERROR_SYMBOL);
		} else if (status.indexOf(UPLOADING_STATUS) !== -1){
			small.setAttribute("style", "color:#0a7ac7;");
                        resetClassListFor(statusSymbol);                        
                        addClassTo(statusSymbol, UPLOADING_SYMBOL);
		}
	}
// output file information
// Used initially and on deletions
	function ParseFile() {
    console.log("parsing file");
   	count_ready = 0;
    str_msg="<li style=\"background: rgb(224, 224, 195);\"></li>";
    if(count !=null && count>=1){
      // process all File objects
        for (var i = 0; i<count; i++) {
            str_rgb="background: rgb(254, 254, 252);";
           // if((i%2)==0){
           //   str_rgb="background: rgb(255, 255, 251);";
           // }
            if(filesdata[i]["status"].indexOf(UPLOADED_STATUS)!=-1){
                  str_msg = str_msg + "<li id='fileSWFUpload_0_" + i + "' style='" + str_rgb + "'>" +
                                          "<div class=\"qContainer\">" +
                                      			"<div id='statusSWFUpload_0_" + i + "' class=\"statusicon ui-icon ui-icon-check\"></div>" +
                                      			"<div id='cancelSWFUpload_0_" + i + "' class=\"cancelicon ui-icon ui-icon-circle-close\"  style=\"display: none;\" value='" + i + "'></div>" +
                                      			"<div id='pbarSWFUpload_0_" + i + "' class=\"ui-progressbar ui-widget ui-widget-content ui-corner-all\" role=\"progressbar\" aria-valuemin='0' aria-valuemax='100' aria-valuenow='100'>" +
                                      				"<span class=\"pblabel\">" + filesdata[i]["name"] + " <small id='smallSWFUpload_0_" + i + "'>[" + filesdata[i]["size"] + " KB] "+ " | " + filesdata[i]["status"] + "</small></span>" +
                                      				"<div class=\"ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right\" style=\"display: block; width: 100%;\"></div>" +
                                      			"</div>" +
                                    		"</div>" +
                                    	"</li>" ;                 
              }else if(filesdata[i]["status"].indexOf(ERROR_STATUS)!=-1){
                str_msg = str_msg + "<li id='fileSWFUpload_0_" + i + "' style='" + str_rgb + "'>" +
                                      "<div class=\"qContainer\">" +
                                  			"<div id='statusSWFUpload_0_" + i + "' class=\"statusicon ui-icon ui-icon-notice\"></div>" +
                                  			"<div id='cancelSWFUpload_0_" + i + "' class=\"cancelicon ui-icon ui-icon-circle-close\"  value='" + i + "'></div>" +
                                  			"<div id='pbarSWFUpload_0_" + i + "' class=\"ui-progressbar ui-widget ui-widget-content ui-corner-all\" role=\"progressbar\" aria-valuemin='0' aria-valuemax='100' aria-valuenow='0'>" +
                                  				"<span class=\"pblabel\">" + filesdata[i]["name"] + " <small id='smallSWFUpload_0_" + i + "'>[" + filesdata[i]["size"] + " KB] "+ " |<font color=red> " + filesdata[i]["status"] + "</font></small></span>" +
                                  				"<div class=\"ui-progressbar-value ui-widget-header ui-corner-left\" style=\"display: none; width: 0%;\"></div>" +
                                  			"</div>" +
                                		"</div>" +
                                	"</li>" ;
              } else{
                str_msg = str_msg + "<li id='fileSWFUpload_0_" + i + "' style='" + str_rgb + "'>" +
                                      "<div class=\"qContainer\">" +
                                  			"<div id='statusSWFUpload_0_" + i + "' class=\"statusicon ui-icon ui-icon-document\"></div>" +
                                  			"<div id='cancelSWFUpload_0_" + i + "' class=\"cancelicon ui-icon ui-icon-circle-close\"  value='" + i + "'></div>" +
                                  			"<div id='pbarSWFUpload_0_" + i + "' class=\"ui-progressbar ui-widget ui-widget-content ui-corner-all\" role=\"progressbar\" aria-valuemin='0' aria-valuemax='100' aria-valuenow='0'>" +
                                  				"<span class=\"pblabel\">" + filesdata[i]["name"] + " <small id='smallSWFUpload_0_" + i + "'>[" + filesdata[i]["size"] + " KB] "+ " | " + filesdata[i]["status"] + "</small></span>" +
                                  				"<div class=\"ui-progressbar-value ui-widget-header ui-corner-left\" style=\"display: none; width: 0%;\"></div>" +
                                  			"</div>" +
                                		"</div>" +
                                	"</li>" ;
                 count_ready++;    
              }            
          }
      }
       if(count_ready>0){
         enableUploadbtn();
       }else{
         disableUploadbtn();
       }
      var queueArea=$id("uploadQueue") 
      queueArea.innerHTML = str_msg;
      for (var i = 0; i<count; i++) {
        $id("cancelSWFUpload_0_" + i ).addEventListener("click",removeFileData , false);
      }
 
	}
	
	function toggleWaitCursor(){
		className = document.body.className;
		document.body.className = className == "" ? "waiting" : "";
	}

	
	function uploadFiles(){
		toggleWaitCursor();
		uploadNext(-1);
	}
	
	function uploadNext(last){
	    var ind = last + 1;
	    if (filesdata[ind] === undefined){
	    	//no more files to upload
	    	verifySuccessfulUploads(ind);
            toggleWaitCursor();
	    	return;
	    }
	    filesdata[ind]["status"] = UPLOADING_STATUS;
	    parseOneFile(ind);
	    var formData = fixFormData(ind);
	    var xhr = new XMLHttpRequest();
	    xhr.callback = uploadNext;
	    xhr.arguments = {"index": ind};
	    xhr.onload = xhrSuccess;
	    xhr.onerror = xhrError;
	    xhr.open('POST', $id("upload").action, true);
	    xhr.setRequestHeader("X_FILENAME", filesdata[ind]["name"]);
	    xhr.send(formData);
	}
	
	function xhrSuccess(){
		var str_success = "\"code\":0";
		var ind = this.arguments["index"];
		filesdata[ind]["status"] = this.responseText
		                               .indexOf(str_success) !== -1 ? UPLOADED_STATUS : ERROR_STATUS;
		parseOneFile(ind);
		this.callback(ind);
	}
	
	function xhrError(){
		console.log(this.statusText);
		var ind = this.arguments["index"];
		filesdata[ind]["status"] = ERROR_STATUS;
		parseOneFile(ind);
		this.callback(ind);
	}
	
	function verifySuccessfulUploads(ind){
		//make sure that each of the file's status is Uploaded
		for (var i = 0; i < ind; i++){
			var status = filesdata[i]["status"];
			if (status.indexOf(ERROR_STATUS) !== -1){
				//one or more files failed
				OutputAlert(ERROR_MESSAGE, ONE_OR_MORE_FILES_FAILED);
				return;
			}
		}
		OutputAlert(SUCCESS_MESSAGE, ALL_FILES_UPLOADED);
	}

  function fixFormData(ind) {
    console.log("inside fixFormData");
    filesdata[ind]["status"]= UPLOADING_STATUS; 
    var formElement = $id("upload");
    var formData = new FormData(formElement);
    formData.append('Filedata', filesdata[ind]["file"]);
    formData.append('docauthor', $id("authorFrom").value);
    formData.append('addressee', $id("addresseeTo").value);
    console.log($id("level2select").options[$id("level2select").selectedIndex].value);
    if (!$id("level2select").disabled && $id("level2select").options[$id("level2select").selectedIndex].value!="") {
        formData.append('level2name', $id("level2select").options[$id("level2select").selectedIndex].text);
        formData.append('level2id', $id("level2select").options[$id("level2select").selectedIndex].value);
        console.log($id("level2select").options[$id("level2select").selectedIndex].text);
        console.log($id("level2select").options[$id("level2select").selectedIndex].value);
    }
    if (!$id("level3select").disabled && $id("level3select").options[$id("level3select").selectedIndex].value != "") {
        formData.append('level3name', $id("level3select").options[$id("level3select").selectedIndex].text);
        formData.append('level3id', $id("level3select").options[$id("level3select").selectedIndex].value);
        console.log($id("level3select").options[$id("level3select").selectedIndex].text);
        console.log($id("level3select").options[$id("level3select").selectedIndex].value);
    }
        
    var sensitiveFlag = "0";
    if($id("sensitiveyes").checked){
        sensitiveFlag="1";
    }
    formData.append('sensitiveflag', sensitiveFlag);
    if (sensitiveFlag == "1") {
        formData.append('sensitiverat', $id("reason").options[$id("reason").selectedIndex].value);
    }
    
    return formData;
  }



	// upload JPEG files
/*	function UploadFile(file) {

		// following line is not necessary: prevents running on SitePoint servers
		if (location.host.indexOf("sitepointstatic") >= 0) return

		var xhr = new XMLHttpRequest();
		if (xhr.upload && (file.type == "image/jpeg" || file.type == "text/txt") && file.size <= $id("MAX_FILE_SIZE").value) {

			// create progress bar
			var o = $id("progress");
			var progress = o.appendChild(document.createElement("p"));
			progress.appendChild(document.createTextNode("upload " + file.name));


			// progress bar
			xhr.upload.addEventListener("progress", function(e) {
				var pc = parseInt(100 - (e.loaded / e.total * 100));
				progress.style.backgroundPosition = pc + "% 0";
			}, false);

			// file received/failed
			xhr.onreadystatechange = function(e) {
				if (xhr.readyState == 4) {
					progress.className = (xhr.status == 200 ? "success" : "failure");
				}
			};

			// start upload
			xhr.open("POST", $id("upload").action, true);
			xhr.setRequestHeader("X_FILENAME", file.name);
			xhr.send(file);

		} 

	}
*/

	// initialize
	function Init() {
 
    var currentDate = new Date();
    $id("docdate").value=(currentDate.getMonth() + 1)+ "/" +  currentDate.getDate() + "/" + currentDate.getFullYear();
    
    
    disableUploadbtn();
    
		var fileselect = $id("Filedata"),
	//	filedrag = $id("filedrag"),
    queueArea=$id("queueArea"),
    level1select = $id("level1select");
		
		// file select
		fileselect.addEventListener("change", FileSelectHandler, false);
   level1select.addEventListener("change", Selectlvl1Handler, false);
   
		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {

			// file drop
		/*	filedrag.addEventListener("dragover", FileDragHover, false);
			filedrag.addEventListener("dragleave", FileDragHover, false);
      filedrag.addEventListener("drag", FileDragHover, false);
      filedrag.addEventListener("dragend", FileDragHover, false);
      filedrag.addEventListener("dragenter", FileDragHover, false);
      filedrag.addEventListener("dragstart", FileDragHover, false);
			filedrag.addEventListener("drop", FileSelectHandler, false);
			filedrag.style.display = "block";
      */
      queueArea.addEventListener("dragover", FileDragHover, false);
			queueArea.addEventListener("dragleave", FileDragHover, false);
      queueArea.addEventListener("drag", FileDragHover, false);
      queueArea.addEventListener("dragend", FileDragHover, false);
      queueArea.addEventListener("dragenter", FileDragHover, false);
      queueArea.addEventListener("dragstart", FileDragHover, false);
			queueArea.addEventListener("drop", FileSelectHandler, false);
      
			// remove submit button
			//submitbutton.style.display = "none";
		}

	}

	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		Init();
	}


})();
