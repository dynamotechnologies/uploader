<?php

require_once "../../../config/uploader-conf.php";

if ((!defined("DATAMART_PREFIX")) ||
    (!defined("DATAMART_AUTH"))) {
	echo "<H1>Error loading utils, check configs</H1>";
        exit(0);
} 

$httpcode = null;

function datamart_get($url) {
  global $httpcode;
  $httpcode = null;

  $ch=curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, DATAMART_AUTH);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  $xml = curl_exec($ch);
  $info = curl_getinfo($ch);
  $httpcode = $info["http_code"];
  curl_close($ch);
  return $xml;
}

/*
 *	Test connection to datamart, return HTTP response code
 */
function datamart_status() {
  global $httpcode;
  $url = DATAMART_PREFIX . "/ref/documentsensitivityrationales/";
  $resp = datamart_get($url);
  return $httpcode;
}

/*
 *	getContainerTree($dom)
 *
 *	Return tree of PHP arrays
 */
function getContainerTree($node)
{
  $me = array();
  $me['order'] = (int)$node['order'];
  $me['label'] = (string)$node['label'];
  $me['contid'] = (int)$node['contid'];

  $contents = array();
  foreach ($node->container as $container) {
    $contents[] = getContainerTree($container);
  }

  $me['contents'] = $contents;
  return $me;
}

/*
 *	getcontainers($projectid)
 *
 *	Return Containers as XML DOM
 */
function getcontainers($projectid) {
  global $httpcode;

  $url = DATAMART_PREFIX . "/projects/nepa/${projectid}/containers/template";
  $xml = datamart_get($url);

  if (is_null($xml)) {
    throw new Exception("No response for project ${projectid}");
  }
  if (intval($httpcode) == 404) {
    throw new Exception("Project ${projectid} not found");
  }
$fp = fopen("/var/tmp/stupidlog.txt", "w");
fwrite($fp, $xml);
fclose($fp);


  $dom = new SimpleXMLElement($xml);
  return $dom;
}

/*
 *	Usage: format_DSR_as_OPTIONS($dom)
 *
 *	Format Document Sensitivity Reasons as string of HTML <OPTION> elements
 */
function format_DSR_as_OPTIONS($node)
{
  $ar = array();
  $order = array();
  foreach ($node->documentsensitivityrationale as $dsr) {
    $id = (int)$dsr->id;
    $si = (int)$dsr->sensitivityid;
    $de = (string)htmlentities($dsr->description);
    $do = (int)$dsr->displayorder;
    $ar[$id] = "<option value='$si'>$de</option>\n";
    $order[$id] = $do;
  }
  asort($order, SORT_NUMERIC);
  $result = "";
  foreach ($order as $key => $val) {
    $result .= $ar[$key];
  }
  return $result;
}

/*
 *	Usage: getRationaleList($dom)
 *
 *	Return Document Sensitivity Rationales as PHP list
 *	of sensitivityid / description
 */
function getRationaleList($dom) {
  $ar = array();
  foreach ($dom->documentsensitivityrationale as $dsr) {
    $si = (int)$dsr->sensitivityid;
    $de = (string)htmlentities($dsr->description);
    $ar[$si] = $de;
  }
  return $ar;
}

/*
 *	Usage: getreasons()
 *
 *	Return Document Sensitivity Reasons as XML DOM
 */
function getreasons() {
  $url = DATAMART_PREFIX . "/ref/documentsensitivityrationales/";
  $xml = datamart_get($url);
  $dom = new SimpleXMLElement($xml);
  return $dom;
}

/*
 *	Usage: getUser()
 *
 *	Return User record as XML DOM
 */
function getUser($shortname) {
  global $httpcode;

  $url = DATAMART_PREFIX . "/users/${shortname}";
  $xml = datamart_get($url);

  if (is_null($xml)) {
    throw new Exception("No response for user ${shortname}");
  }
  if (intval($httpcode) == 404) {
    throw new Exception("User ${shortname} not found");
  }

  $dom = new SimpleXMLElement($xml);
  return $dom;
}

?>
