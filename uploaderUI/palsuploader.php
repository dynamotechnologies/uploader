<?php
function _die($err1, $err2 = "") {
	$PALSHELP = "pals-help@fs.fed.us";
	echo "<H1>$err1</H1>";
	echo "<H2>$err2</H2>";
	echo "<H3>Please contact <A HREF='mailto:$PALSHELP'>$PALSHELP</A></H3>";
	exit(0);
}

if (!file_exists('/var/www/config/uploader-conf.php')) {
	_die("System error, check config file");
}

require_once('/var/www/config/uploader-conf.php');
require_once('datamart_utils.php');

/* Stop IE8 from modifying the page for XSS */
header('X-XSS-Protection: 0');

if (!defined("UPDATEPALSURL")
 || !defined("FILERECORDERHOST")
 || !defined("BROKERDIR")) {
	_die("System error, check configs");
}

$dm_code = datamart_status();
if ($dm_code != 200) {
	echo "<H1>Datamart error, cannot continue</H1>";
	if ($dm_code == 0) {
		_die(	"Datamart error, cannot continue",
			"Server may be down");
	} else {
		_die(	"Datamart error, cannot continue",
			"Returning code $dm_code");
	}
}

$updatePALSURL = UPDATEPALSURL;

/*
Should be getting
		"appsysid"   : "01",
		"appdoctype" : "Scoping Documents",
		    "author" : "PALS user shortname",
		 "projectid" : "PR2987",
		     "jobid" : "123456",
as GET parameters

appsysid is either 01 (launched from Documents tab) or 05 (PFM)

appdoctype has to match one of the top-level categories in the container tree
or we won't be able to display the groups and subgroups

2013-10-08 If appdoctype is missing then display a select-category field
and allow user to pick the lead category as well
*/

// CONFIGS
$jobOpenURL  = "/uploader/jobopen.php";
//$jobOpenURL  = "/missing-file";		  // simulate server error
//$jobOpenURL  = "http://localhost/missing-file"; // simulate net err
$jobUpdateURL  = "/uploader/jobupdate.php";
$jobCloseURL = "/uploader/jobclose.php";

$appsysid = $_GET['appsysid'];
$appdoctype = "";
if (isset($_GET['appdoctype'])) {
    $appdoctype = $_GET['appdoctype'];
}
$author = $_GET['author'];
$projectid = $_GET['projectid'];

if (isset($_GET['jobid'])){
	$jobid = $_GET['jobid'];
}else{
	// Make up a fileID
	$basepart = strtoupper(base_convert(time(), 10, 16));
	$randompart = strtoupper(base_convert(rand(0, 255), 10, 16));
	$jobid = $basepart . $randompart;
}


$email = "";

try {
  $user = getUser($author);
  $email = strval($user->email);
} catch (Exception $e) {
    _die(	"Couldn't retrieve user record from Datamart, cannot continue",
		"Returning error: " . $e->getMessage());
}
if (trim($email) == "") {
    _die(	"Couldn't find a valid user email address, cannot continue",
		"");
}

$reason_OPTIONS;
$container_JSON;

try {
	$dom = getreasons();
	$reason_OPTIONS = format_DSR_as_OPTIONS($dom);
} catch (Exception $e) {
	_die("Error loading sensitivity reasons", $e->getMessage());
}

try {
	$dom = getcontainers($projectid);
	$container_JSON = json_encode(getContainerTree($dom), JSON_HEX_APOS|JSON_HEX_QUOT);
} catch (Exception $e) {
	_die(	"Error loading containers for project ${projectid}",
		$e->getMessage());
}

if (strlen($jobid) > 30) {
	_die("Bad job ID: " . htmlentities($jobid));
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Uploading documents</title>

<link type="text/css" href="../jquery/css/custom-theme/jquery-ui-1.8.18.custom.css" rel="Stylesheet"/>
<link rel="stylesheet" type="text/css" href="upload.css"/>
<link type="text/css" href="qtip/jquery.qtip.css" rel="Stylesheet" />
<?php
/*
 * After combining, replace the above with
<link rel="stylesheet" type="text/css" href="combined-min.css">
 */
?>

<script type="text/javascript">

  var _gaq = _gaq || [];

<?php if (defined("GA_TRACKER_ID")) { ?>

  _gaq.push(['_setAccount', '<?php echo GA_TRACKER_ID; ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

<?php } ?>

</script>

</head>
<body oncontextmenu="return false;">

	<div id="divURLUploader">
		<div class="dialogTitle">URL Document Uploader</div>
		<form id="urltarget" enctype="multipart/form-data" action="/uploader/urluploader/target.php" method="post">
			<input type="file" name="Filedata"></input>
			<input type="submit" value="Upload"></input>
			<input type="hidden" name="jobid"/>
			<input type="hidden" name="author"/>
			<input type="hidden" name="appsysid"/>
			<input type="hidden" name="projectid"/>
			<input type="hidden" name="email"/>
		</form>
		<div id="urlup_progressbar"></div>
		<hr/>
		<div class="urlup_txt">
  		<div class="header">About</div>
  		<div class="body">The URL Document Uploader allows documents with publicly accessible URLs to be pulled into PALS along with standard document attributes.</div>
		</div>
		<br/>
		<br/>
		<div class="urlup_txt">
  		<div class="header">How to Use</div>
  		<div class="body">To use this capability, populate the Excel Template (<a href="../docs/get.php?doc=urltemplate">Link to Excel Template</a>) with the URL of the documents to be uploaded, required document attributes, and other attributes as they apply and save to your hard drive.  Then use the Browse button above to select the file and select Upload.  If there are no errors, the documents will be uploaded to PALS along with the attributes provided.</div>
  		<div class="body">For further instructions on the use of the Excel Template please refer to the Detail Instructions document (<a href="../docs/get.php?doc=urlinstructions">Link to Detailed Instructions</a>)</div>
		</div>
	</div>

	<div id="divExit" style="min-width:800px">
		<div id="urlUploaderBtn" class="btn">URL Uploader</div>
		<div id="exitBtn" class="btn">Close this window</div>
<?php /*
		<form id="hiddenExitForm" method="get"
			target="hiddenIFrame"
			action="<?php echo "${updatePALSURL}"; ?>">
			<input name="jobid" type="text"
			 value="<?php echo "${jobid}"; ?>">
			<input id="hiddenExitBtn" type="submit"
		 	value="Hidden exit button"/>
		</form>
		<iframe id="hiddenIFrame" name="hiddenIFrame"></iframe>
*/ ?>
	</div>
	<div id="warningDiv" style="min-width:800px"><?php // warnings go here ?></div>

	<div id="titleBanner" style="min-width:800px">
		<img id="clipboardImg" src="assets/clipboard.gif" alt="clipboard"/>
		<div id="titleStripe">
			<div id="title"></div>
		</div>
	</div>

<div id="content_old">
<!--

	<div id="divLoadingContent">

	<noscript>
		<div>
			SWFUpload could not load because Javascript
			is disabled.  Please enable Javascript to
			enable uploading.
		</div>
	</noscript>

	<div id="targetCategory">
	<span>Select Target Category</span>
	<div id="selectCategory">
		<select id="level1select"><option/></select>
	</div>
	</div>
	<div id="targetGroup">
	<span>Select Target Group and Sub-Group (if applicable)</span>
	<div id="selectGroup">
		<select id="level2select"><option/></select>
		<select id="level3select"><option/></select>
	</div>
	</div>

	<div id="divQueue" class="loaderContent">
		<div id="queueArea">
			<div id="queueTitle">Selected Documents</div>
			<ul id="uploadQueue" class="selectable">
				<li/>
			</ul>
		</div>

		<div id="applyMetadata">
		  <span>Add Metadata (affects all documents that are uploaded)</span>
        <?php /*<div> Date Created<input id="createDate" type="text"/></div> */?>
        
        <input id="createDateAlt" type="hidden"/>
        <br/>
          &nbsp;
	      <?php /* <input id="dateLabel" type="text" value="Date Uploaded"></input>*/?>
          <select id="dateLabel">
          <option selected>Date Uploaded</option>
          <option>Signed On</option>
          <option>Published On</option>
          <option>Delivered On</option>
          <option>Completed On</option>
          </select>
		  
		  
		  <input id="docDate" type="text"></input>
		  <br><br>
		  <div>
          Addressee/To<input id="addresseeTo" type="text"/>
        &nbsp;    Author/From<input id="authorFrom" type="text"/>
		<br><br>
		  Marked as "sensitive":
		    <input type="radio" name="sensitive" value="1"/>Yes
		    <input type="radio" name="sensitive" value="0" checked="checked"/>No
           &nbsp; 
		 
		  
		  Reason
		    <select id="reason">
		      <option selected="selected" value="">Choose one</option>
		      <?php echo $reason_OPTIONS; ?>
		    </select>
		  </div>
		
	</div>	<?php /* divLoadingContent */ ?>

	</div>
-->
	<div id="divControls" class="loaderContent1">
		<form id="form1" action="index.php" method="post"
		 enctype="multipart/form-data">
			<div id="fileSelectBtn1">

				<div id="spanButtonPlaceHolder">(Position of Flash uploader control)</div>

			</div>
		</form>
<!--
		<div id="startUploadBtn" class="btn">
			Upload files
		</div>
		<div id="legend">
			<div id="legendHeader">Legend</div>
			<div id="legendContent">
			<div><span id="iconQueued"></span>File Queued</div>
			<div><span id="iconProgress"></span>In Progress</div>
			<div><span id="iconError"></span>Error</div>
			<div><span id="iconDone"></span>Upload Complete</div>
			<div><span id="iconCancel"></span>Cancel File</div>
			</div>
		</div>
-->
	</div>
<!--
	<div id="divAlternateContent">
	SWFUpload could not load.
	You may need to install or upgrade Flash Player.
	Visit the <a href="">Adobe website</a> to get the latest Flash Player.
	</div>
-->
</div>	<?php /* div id="content" */ ?>
<?php /* STS CODE STARTED */ ?>
</br>
<div id="messages">
</div>

<div  id="content" style="min-width:900px;max-width:900px">
  <div id="divLoadingContent">
      <form id="upload" action="/uploader/swftarget.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="jobid" value="<?php echo $jobid; ?>"/>
    			<input type="hidden" name="author" value="<?php echo $author; ?>"/>
    			<input type="hidden" name="appsysid" value="<?php echo $appsysid; ?>"/>
    			<input type="hidden" name="projectid" value="<?php echo $projectid; ?>"/>
    			<input type="hidden" name="email" value="<?php echo $email; ?>"/>
          <input type="hidden" name="appdoctype" id="appdoctype"/>
          <input type="hidden" name="Filename" />
         <!-- <input type="hidden" name="jobOpenURL" value="<?php echo $jobOpenURL; ?>"/>
          <input type="hidden" name="jobUpdateURL" value="<?php echo $jobUpdateURL; ?>"/>
          <input type="hidden" name="jobCloseURL" value="<?php echo $jobCloseURL; ?>"/>
          <input type="hidden" name="jobNotifyURL" value="<?php echo $updatePALSURL; ?>"/>
          <input type="hidden" name="jobStatus" value="U"/>
          -->
      
        	<noscript>
        		<div>
        			SWFUpload could not load because Javascript
        			is disabled.  Please enable Javascript to
        			enable uploading.
        		</div>
        	</noscript>
          <div id="targetCategory">
          	<span>Select Target Category</span>
          	<div id="selectCategory">
          		<select id="level1select"><option/></select>
          	</div>
        	</div>
        	<div id="targetGroup">
          	<span>Select Target Group and Sub-Group (if applicable)</span>
          	<div id="selectGroup">
          		<select id="level2select"><option/></select>
          		<select id="level3select"><option/></select>
          	</div>
        	</div> 
        	<div id="divQueue" class="loaderContent">
        		<div id="queueArea">
        			<div id="queueTitle">Selected Documents</div>
        		  <ul id="uploadQueue" class="selectable">
        			  <li style="background: rgb(224, 224, 195);"></li>
        		  </ul>
        	  </div>
           	<div id="applyMetadata">
        		  <span>Add Metadata (affects all documents that are uploaded)</span>
                <?php /*<div> Date Created<input id="createDate" type="text"/></div> */?>
                <input id="createDateAlt" type="hidden"/></br>
        	      <?php /* <input id="dateLabel" type="text" value="Date Uploaded"></input>*/?>
                  <select id="datelabel" name="datelabel" name="datelabel">
                  <option selected>Date Uploaded</option>
                  <option>Signed On</option>
                  <option>Published On</option>
                  <option>Delivered On</option>
                  <option>Completed On</option>
                  </select>
                   
        		      <input id="docdate" name="docdate" type="text"></input>
                  
                  </br></br>
            		  <div>
                      Addressee/To<input id="addresseeTo" type="text"/>&nbsp;
                      Author/From<input id="authorFrom" type="text"/> <br><br>
            		      Marked as "sensitive":
                		    <input type="radio" name="sensitive" id="sensitiveyes" value="1"/>Yes
                		    <input type="radio" name="sensitive"  id="sensitiveno" value="0" checked="checked"/>No
                           &nbsp; 		  
            		      Reason
                		    <select id="reason">
                		      <option selected="selected" value="">Choose one</option>
                		      <?php echo $reason_OPTIONS; ?>
                		    </select>
            		  </div>
            	</div><?php /* divapplyMetadata */ ?>
          </div><?php /* divloaderContent */ ?>
          <div id="divControls" class="loaderContent">
            <div id="Filebrowse">
              <label for="Filedata" id="lblFiledata" class="custom-file-upload btn "><i class="fa fa-cloud-upload" ></i> <strong>Browse</strong></label>
      	      <input type="file" id="Filedata" name="Filedata" multiple="multiple"/>
            </div><br><br>
            <div id="StartUpload">
              
            </div>
            <div id="legend" class="ui-widget">
        			<div id="legendHeader" class="ui-widget-header" style="color: rgb(240, 240, 240); background: rgb(71, 87, 29);">Legend</div>
        			<div id="legendContent" class="ui-widget-content">
        			<div><span id="iconQueued" class="ui-icon ui-icon-document"></span>File Queued</div>
        			<div style="background: rgb(255, 255, 255);"><span id="iconProgress" class="ui-icon ui-icon-arrowthick-1-e"></span>In Progress</div>
        			<div><span id="iconError" class="ui-icon ui-icon-notice"></span>Error</div>
        			<div style="background: rgb(255, 255, 255);"><span id="iconDone" class="ui-icon ui-icon-check"></span>Upload Complete</div>
        			<div><span id="iconCancel" class="ui-icon ui-icon-circle-close"></span>Cancel File</div>
        			</div>
        		</div>
          </div>
          
    </form>
	</div>	<?php /* divLoadingContent */ ?>
<style>
#Filedata {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
    width:100px;
}
 </style>


<?php /* STS CODE END */ ?>
<script type="text/javascript" src="../jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../jquery/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="qtip/jquery.qtip.js"></script>
<script type="text/javascript" src="core/swfupload.js"></script>
<script type="text/javascript" src="js/swfupload.swfobject.js"></script>
<script type="text/javascript" src="js/swfupload.queue.js"></script>
<script type="text/javascript" src="js/filedrag.js"></script>
<script type="text/javascript">
  $( function() {
    $( "#docdate" ).datepicker();
  } );
  
</script>

<?php
/*
 * After combining, replace the above with
<script type="text/javascript" src="combined-min.js"></script>
 */
?>

<script type="text/javascript">
		var swfu;
		var containers = $.parseJSON('<?php echo $container_JSON; ?>');

		SWFUpload.onload = function() {
			var settings = {
				custom_settings :
				{
				jobID : "<?php echo $jobid; ?>",
				maxFilenameLength : 255,
				appdoctype   : "<?php echo $appdoctype; ?>",
				jobOpenURL   : "<?php echo $jobOpenURL; ?>",
				jobUpdateURL : "<?php echo $jobUpdateURL; ?>",
				jobCloseURL  : "<?php echo $jobCloseURL; ?>",
				jobNotifyURL : "<?php echo $updatePALSURL; ?>",
				jobStatus : "U", <?php // Unknown ?>

				appSysID     : "<?php echo $appsysid; ?>",
				author       : "<?php echo $author; ?>",
				projectID    : "<?php echo $projectid; ?>",
				email        : "<?php echo $email; ?>",

<?php				 // interval timers ?>

				jobOpenTimer : 0,
				openJobRetries : 0,
				jobUpdateTimer : 0, <?php // network timer ?>
				updateJobRetries : 0,
				inactivityTimer : 0
				},
		flash_url : "/uploader/uploaderUI/core/Flash/swfupload.swf",
		upload_url: "/uploader/swftarget.php",

			<?php	// debug : true, ?>

		button_placeholder_id: "spanButtonPlaceHolder",
		button_window_mode : SWFUpload.WINDOW_MODE.OPAQUE,
	<?php	// button_image_url: "assets/testimage.png", ?>
		button_image_url: "assets/swfu_112x188.gif",
		button_cursor : SWFUpload.CURSOR.HAND,

				post_params :
				{
				"appsysid" : "<?php echo $appsysid; ?>",
				"appdoctype" : "<?php echo $appdoctype; ?>",
				"author" : "<?php echo $author; ?>",
				"projectid" : "<?php echo $projectid; ?>",
				"jobid" : "<?php echo $jobid; ?>"
				},

<?php		// The event handler functions are defined in handlers.js ?>

                                file_queued_handler : fileQueued,
                                file_queue_error_handler : fileQueueError,
                                file_dialog_complete_handler : fileDialogComplete,
                                upload_start_handler : uploadStart,
                                upload_progress_handler : uploadProgress,
                                upload_error_handler : uploadError,
                                upload_success_handler : uploadSuccess,
			<?php	// upload_complete_handler : uploadComplete, ?>
                                queue_complete_handler : queueComplete,

<?php		// Event handler functions for SWFObject plugin ?>

<?php
/*
 *				// pre_load only fires if minimum version of
 *				// Flash Player is found.  I'm not sure why
 *				// this would be useful
 *		                // swfupload_pre_load_handler: swfUploadPreLoad,
 */
?>

				minimum_flash_version : "9.0.28",
		                swfupload_loaded_handler : swfUploadLoaded,

<?php
/*
 *				// load_failed only fires if minimum version
 *				// of Flash Player is not met, but does not
 *				// fire if SWF is missing, corrupted, or if
 *				// browser has bugs
 */
?>
                		swfupload_load_failed_handler : swfUploadLoadFailed,

<?php		// Validation ?>
				file_types_description : "All document types", 
				file_size_limit : "80 MB"
			};

			swfu = new SWFUpload(settings);
			$('#content').data('swfu', swfu);
			$('#content').data('containers', containers);
		};
</script>

<script type="text/javascript" src="js/uploadprogress.js"></script>
<script type="text/javascript" src="js/handlers_drag.js"></script>
</div>	<?php /* divContent */ ?>

</body>
</html>
