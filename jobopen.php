<?php

require_once("/var/www/config/uploader-conf.php");

$rechost = FILERECORDERHOST;

if ($rechost === "") {
        echo "jobopen failed, please configure uploader";
        exit(0);
}

/*
 * Get jobid from _POST
 * Get IP addr from _SERVER
 * POST new message to FILERECORDERHOST/filerecorder/openJob.php
 * return response
 * (0 for OK, anything else is an error)
 */

$jobid = $_POST['jobid'];
$ipaddr = $_SERVER['REMOTE_ADDR'];

$postURL = "https://$rechost/filerecorder/openJob.php";

$postdata = http_build_query(
  array(
    'jobid' => $jobid,
    'reqip' => $ipaddr
  )
);

$opts = array('http' =>
  array(
    'method' => 'POST',
    'header' => 'Content-type: application/x-www-form-urlencoded',
    'content' => $postdata
  )
);

$context = stream_context_create($opts);

$result = file_get_contents($postURL, false, $context);

print $result;
?>
