<?php
/*
 *	URL uploader target
 *
 *	This script processes an Excel file in
 *	/var/www/html/tmp/urluploader/[jobid]/urls.xlsx
 *	containing URLs and metadata of files to be checked in,
 *	and reuses the swftarget.php script to check in new files
 */
require_once '/var/www/common/phpexcel/Classes/PHPExcel.php';
require_once '/var/www/config/uploader-conf.php';
require_once '/var/www/html/uploader/uploaderUI/datamart_utils.php';
require_once 'target_common.php';
require_once '/var/www/common/swiftmailer/lib/swift_required.php';

require_once 'cleanup.php';	// Purge old jobs

/*
 *	Job data is stored in /var/www/html/tmp/urluploader/[jobid]/
 *
 *	Status for each individual file needs to be recorded in a
 *	report file, which will be returned to user at the end of the job
 *	Current design says that this report is in the form of a CSV but
 *	I think we're pretty flexible if this needs to change
 *
 *	/var/www/html/tmp/urluploader/[jobid]/report.csv
 *
 *	Job status needs to be stored in an externally-visible web file,
 *	which will be parsed by the UI and displayed as a progress bar,
 *	this includes current overall job health and progress
 *
 *	/var/www/html/tmp/urluploader/[jobid]/status.js
 */

$email = $_POST['email'];
if (isEmpty($email)) { throw new Exception("email address is missing!"); }

$response = array();
try {
  $errcount = main();
  $response['msg'] = "Job complete with $errcount errors";
  $response['errors'] = $errcount;
  $response['report'] = "$REPORTFILE_URL";	// see target_common.php
  sendConfirmationEmail($email, $errcount);
} catch (Exception $e) {
  // fatal error, could not complete processing
  $response['msg'] = "Uploader exited with error: " . $e->getMessage();
  $response['errors'] = -1;
  // test report file
  if (filesize($REPORTFILE)) {
    $response['report'] = "$REPORTFILE_URL";	// see target_common.php
  } else {
    $response['report'] = '';
  }
  sendFailureEmail($email, $e->errorMsg());
}
echo json_encode($response);	// JSON response to target.php



function main() {
  global $CONTAINERS_TREE;
  global $RATIONALES_LIST;
  
  /*
   *	Validate form parameters
   */
  $jobid = $_POST['jobid'];
  $author = $_POST['author'];
  $appsysid = $_POST['appsysid'];
  $projectid = $_POST['projectid'];

  if (isEmpty($jobid)) { throw new Exception("jobid is empty!"); }
  if (isEmpty($author)) { throw new Exception("author is empty!"); }
  if (isEmpty($appsysid)) { throw new Exception("appsysid is empty!"); }
  if (isEmpty($projectid)) { throw new Exception("projectid is empty!"); }

  if (strlen($author) > MAX_AUTHOR_LEN) {
    throw new Exception("Author too long, > " . MAX_AUTHOR_LEN);
  }
  
  $pubdate = PHPExcel_Shared_Date::PHPToExcel(time());	// make dateserial

  init_globals($jobid);		// from target_common.php

  /*
   *	Make sure job is open
   */
  check_job($jobid);

  /*
   *	Get the container template for this project
   */
  $dom = getcontainers($projectid);
  $CONTAINERS_TREE = getContainerTree($dom);
  
  /*
   *	Get the document sensitivity rationales for this project
   */
  $dom = getreasons();
  $RATIONALES_LIST = getRationaleList($dom);
  
  $objPHPExcel = getPHPExcel();
  $objWorksheet = $objPHPExcel->getActiveSheet();
  
  $highestRowIndex = $objWorksheet->getHighestRow();
  $highestColumnIndex = 12;
  
  // Unfortunately highestRowIndex may contain largely blank lines, so it
  // cannot be used as the last row.  Scan the worksheet to find the last
  // row with a non-empty URL column
  $maxRow = findLastRow($objWorksheet);

  $maxRowCnt = $maxRow - 1;
  updateStatus(0, $maxRowCnt, "Starting process");
  updateReport("row", "url", "status [projectid=$projectid jobid=$jobid]");		// create title row

  $failures = 0;

  // Loop through all spreadsheet rows
  for ($row = 2; $row <= $maxRow; ++$row) {	// rows are 1-based
    $metadata = array();
    // Copy global parameters
    $metadata[JOBID] = $jobid;
    $metadata[APPSYSID] = $appsysid;
    $metadata[AUTHOR] = $author;
    $metadata[PUBDATE] = $pubdate;
    $metadata[PROJECTID] = $projectid;
    $metadata[FILENAME] = null;
  
    $metadata[ROW] = $row;
  
    // Copy spreadsheet row into metadata array
    for ($col = 0; $col <= $highestColumnIndex; ++$col) {	// cols are 0-based
      $cell = getCell($objWorksheet, $row, $col);
      // Make sure current row has a URL, otherwise quit
      if (($col == DOCUMENTURL) && (isEmpty($cell))) {
        $errormsg = "Row $row has no URL, ending job here";
        updateReport($row, $metadata[DOCUMENTURL], $errormsg);
        throw new Exception($errormsg);
      } else {
        $metadata[$col] = $cell;
      }

      // Update the job timestamp to make sure it stays open
      check_job($jobid);
    }
  
    // Validate row data
    $rowCnt = $row - 1;
    updateStatus($rowCnt, $maxRowCnt, "Processing row $rowCnt of $maxRowCnt");
    try {
      $metadata = validate($metadata);
      // row is OK
      $msg = processFile($metadata);
      updateReport($row, $metadata[DOCUMENTURL], $msg);
    } catch (Exception $e) {
      // Row failed validation or processing
      $url = $e->getMessage();
      $msg = $e->getMessage();
      updateStatus($rowCnt, $maxRowCnt, $msg);
      updateReport($row, $metadata[DOCUMENTURL], $msg);
      $failures += 1;
    }
  }

  /*
   *	All done, close job
   */
  close_job($jobid);

  return $failures;
}

function sendFailureEmail($emailaddr, $errormsg) {
    global $REPORTFILE;

    $jobid = $_POST['jobid'];

    $htmlmsg = "<html><body>";
    $htmlmsg .= "<p>Your job could not be completed.</p>";
    $htmlmsg .= "<p>Error: $errormsg</p>";
    $htmlmsg .= "<p>Please see the attached report file for details.</p>";
    $htmlmsg .= "</body></html>";

    $msg = "Your job could not be completed.\n";
    $msg .= "Error: $errormsg\n";
    $msg .= "Please see the attached report file for details.\n";

    $subject = "URL Document Uploader job failed - job $jobid";

    sendMail(array($emailaddr), $subject, $msg, $htmlmsg, array($REPORTFILE));
}

function sendConfirmationEmail($emailaddr, $failurecnt) {
    global $REPORTFILE;

    $jobid = $_POST['jobid'];

    $msg = "";
    $htmlmsg = "<html><body>";

    if ($failurecnt == 0) {
        $status = "success";
        $msg .= "Your job has completed processing without errors.\n";
        $htmlmsg .= "<p>Your job has completed processing without errors.</p>";
    } else {
        $status = "completed with errors";
        $msg .= "Your job has completed processing.\n";
        $msg .= "There were $failurecnt errors.\n";
        $htmlmsg .= "<p>Your job has completed processing.</p>";
        $htmlmsg .= "<p>There were $failurecnt errors.</p>";
    }

    $msg .= "Please see the attached report file for further details.";
    $htmlmsg .= "<p>Please see the attached report file for further details.</p>";
    $htmlmsg .= "</body></html>";

    $subject = "URL Document Uploader $status - job $jobid";

    sendMail(array($emailaddr), $subject, $msg, $htmlmsg, array($REPORTFILE));
}

/*
 *	Send email, with attachments
 *
 *	tolist is an array of email addresses, possibly including names
 *	    (e.g. array("me@localhost.com",
 *	                "you@somewhere.com" => "Your Name");
 *	subject is a string
 *	msg is a string
 *
 *	htmlmsg is an optional version of "msg" in HTML
 *	attachmentlist is an optional array of paths to files to be attached
 */
function sendMail($tolist, $subject, $msg, $htmlmsg = null, $attachmentlist = null) {
    if ($htmlmsg == null) {
        $htmlmsg = "<html><body><pre>$msg</pre></body></html>";
    }

    if ($attachmentlist == null) {
        $attachmentlist = array();
    }

    $message = Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom(array('noreply@fs.fed.us' => "PALS File Uploader"))
        ->setTo($tolist)
        ->setBody($msg)
        ->addPart($htmlmsg, 'text/html');

    // Add any attachments
    foreach ($attachmentlist as $path) {
        $message->attach(Swift_Attachment::fromPath($path));
    }

    $transport = Swift_SmtpTransport::newInstance('localhost', 25);
    $mailer = Swift_Mailer::newInstance($transport);
    $emailsSent = $mailer->send($message);
    // Not currently doing anything with emailsSet, but this should be the
    // number of recipients who are getting mail
}

function getPHPExcel() {
  global $EXCEL_FILE;	// path to the Excel spreadsheet

  try {
    $objReader = PHPExcel_IOFactory::createReaderForFile($EXCEL_FILE);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($EXCEL_FILE);
    return $objPHPExcel;
  } catch (Exception $e) {
    throw new Exception("Error reading file: [" . $e->getMessage() . "]");
  }
}

/*
 *	Scan the worksheet to find the last row with a non-empty URL column
 */
function findLastRow($worksheet) {
  $maxRow = $worksheet->getHighestRow();
  for ($row = 2; $row <= $maxRow; $row++) {
    $cell = getCell($worksheet, $row, DOCUMENTURL);
    if (isEmpty($cell)) {
      return $row - 1;
    }
  }
  return $maxRow;
}

/*
 *	Escape any unsafe characters in the path portion of a URL
 *
 *	I don't think it's likely there will be bad characters (e.g. spaces)
 *	in any other part of the URL, but it's probably something to keep
 *	in mind.
 */
function escape_url($url) {
    $parts = parse_url($url);
    if ($parts == FALSE) {
        throw new Exception ("URL is seriously malformed!");
    }

    $newurl = $parts['scheme'] . '://';

    if ($parts['user'] || $parts['pass']) {
      $newurl .= $parts['user'] . ':' . $parts['pass'] . '@';
    }

    $newurl .= $parts['host'];

    if ($parts['port']) {
      $newurl .= ':' . $parts['port'];
    }

    $newurl .= str_replace("%2F", "/", rawurlencode($parts['path']));
    // (ordinary urlencode changes spaces to + signs instead of %20)

    if ($parts['query']) {
      $newurl .= '?' . $parts['query'];
    }

    if ($parts['fragment']) {
      $newurl .= '#' . $parts['fragment'];
    }

    return $newurl;
}

function processFile($metadata) {
    // Escape URL path unless it contains a percent sign
    $fileURL = $metadata[DOCUMENTURL];
    if (strpos($fileURL, '%') === False) {
      try {
        $fileURL = escape_url($fileURL);
      } catch (Exception $e) {
        return "Could not escape URL: " . $e->getMessage();
      }
    }

    // Attempt to download file
    try {
      $filedata = file_get_contents($fileURL);
      if ($filedata === False) {
        return "Error getting file [ $fileURL ] " . implode(' / ', $http_response_header);
      }
    } catch (Exception $e) {
      return "Exception [" . $e->getMessage() . "] retrieving file";
    }

    // Check in file
    $docid = "UNKNOWN";
    try {
      $docid = checkin($filedata,
              $metadata[FILENAME],
              $metadata[TITLE],
              $metadata[JOBID],
              $metadata[APPSYSID],
              $metadata[DOCTYPE],
              $metadata[AUTHOR],
              $metadata[PROJECTID],
              dateserialToDocdate($metadata[DOCDATE]),
              $metadata[DATELABEL],
              dateserialToDocdate($metadata[DATECREATED]),
              $metadata[GROUPNAME],
              $metadata[GROUPID],
              $metadata[SUBGROUPNAME],
              $metadata[SUBGROUPID],
              $metadata[SENSITIVE],	// true/false, submitted as 1/0
              $metadata[SENSITIVITYRATIONALE],
              $metadata[DOCUMENTAUTHOR],
              $metadata[DOCSUMMARY],
              $metadata[RECIPIENT]);	// addressee
    } catch (Exception $e) {
      return "Exception [" . $e->getMessage() . "] checking in file";
    }

    // Completed OK
    return "Done, docid = $docid";
}

/*
 *	Check in the supplied file (data) and parameters
 *
 *	Throw exception for any errors, otherwise return new docid
 */
function checkin(
    $filedata,	// the file
    $filename,
    $title,
    $jobid,
    $appsysid,
    $appdoctype,
    $author,
    $projectid,
    $docdate,		// date should be formatted as MM/DD/YYYY
    $datelabel,
    $datecreated,	// date should be formatted as MM/DD/YYYY
    $level2name,	// omit if null
    $level2id,		// omit if level2name missing
    $level3name,	// omit if null
    $level3id,		// omit if level3name missing
    $sensitiveflag,	// true/false, submit as "1"/"0"
    $sensitiverat,	// omit if sensitiveflag == false
    $docauthor,		// omit if null
    $docsummary,	// omit if null
    $addressee		// omit if null
    )
{
    global $WWWJOBDIR;
    $tempfile = "$WWWJOBDIR/tmpfile";	// the new file to upload
    // Write data to tempfile
    if (is_file($tempfile)) {
      if (unlink($tempfile) == False) {
        throw new Exception("Unable to delete tempfile $tempfile");
      }
    }

    $fp = fopen($tempfile, "w+");
    fwrite($fp, $filedata);
    fclose($fp);

    // Post everything to swftarget.php
    $postfields = array(
        'Filedata' => new CurlFile($tempfile),	// swftarget expects file in Filedata
        'jobid'		=> $jobid,
        'title'		=> $title,
        'docdate'	=> $docdate,
        'appsysid'	=> $appsysid,
        'appdoctype'	=> $appdoctype,
        'Filename'	=> $filename,
        'author'	=> $author,
        'projectid'	=> $projectid,
        'datelabel'	=> $datelabel,
        'datecreated'	=> $datecreated
    );
    if (isEmpty($level2name) == False) {
        $postfields['level2name'] = $level2name;
        $postfields['level2id'] = $level2id;
    }
    if (isEmpty($level3name) == False) {
        $postfields['level3name'] = $level3name;
        $postfields['level3id'] = $level3id;
    }
    $postfields['sensitiveflag'] = ($sensitiveflag ? "1" : "0");
    if ($sensitiveflag) {
        $postfields['sensitiverat'] = $sensitiverat;
    }
    if (isEmpty($docauthor) == False) {
        $postfields['docauthor'] = $docauthor;
    }
    if (isEmpty($docsummary) == False) {
        $postfields['docsummary'] = $docsummary;
    }
    if (isEmpty($addressee) == False) {
        $postfields['addressee'] = $addressee;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, CHECKIN_TARGET);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $json_result = curl_exec($ch);
    if ($json_result == "") {
      throw new Exception("No response from checkin service, check Stellent connection?");
    }
    if ($json_result === False) {
      $errmsg = "Error calling curl_exec [";
      $errmsg .= "msg: " . curl_error($ch) . ", ";
      $errmsg .= "errno: " . curl_errno($ch);
      $errmsg .= "]";
      throw new Exception($errmsg);
    }
    curl_close ($ch);

    // Cleanup
    if (is_file($tempfile)) {
      if (unlink($tempfile) == False) {
        throw new Exception("Unable to delete tempfile $tempfile");
      }
    }

    // $json_result is the response from swftarget.php, which is a JSON array
    // with "code" and "message" indexes
    // The 2nd parameter to json_decode requests objects to be converted to
    // PHP arrays
    $result = json_decode($json_result, True);
    if ($result['code'] != SWF_OK) {
      throw new Exception("Checkin error: " . $result['message']);
    } else {
      // 'message' contains new doc ID
      return $result['message'];
    }
}

/*
 *	Validate metadata fields
 *	This returns the metadata array, which may be altered
 */
function validate($metadata) {
  $sensitiveFlag = False;
  $projectid = $metadata[PROJECTID];
  foreach ($metadata as $idx=>$col) {
    switch ($idx) {
    case DOCUMENTURL:
      if (isEmpty($col)) {
        throw new Exception("Missing document URL");
      }
      // filename len <= 255
      // get filename from URL
      $path = parse_url($col, PHP_URL_PATH);
      $filename = basename($path);
      $metadata[FILENAME] = $filename;
      // check filename length
      if (strlen($filename) > MAX_FILENAME_LEN) {
        throw new Exception("URL filename too long, > " . MAX_FILENAME_LEN);
      }
      break;
    case DOCTYPE:
      // doctype not null
      if (isEmpty($col)) {
        throw new Exception("Missing doctype");
      }
      break;
    case TITLE:
      if (isEmpty($col)) {
        // default to filename
        $metadata[TITLE] = $metadata[FILENAME];
      }
      $title = $metadata[TITLE];
      // title len <= 200
      if (strlen($title) > MAX_TITLE_LEN) {
        throw new Exception("Title too long, > " . MAX_TITLE_LEN);
      }
      break;
    case DOCDATE:
      if (isEmpty($col)) {
        // default to pubdate
        $metadata[DOCDATE] = $metadata[PUBDATE];
      }
      // check format
      $docdate = $metadata[DOCDATE];
      if (!is_numeric($docdate)) {
        throw new Exception("Docdate $docdate is not a dateserial, check type in spreadsheet");
      }
      break;
    case DATELABEL:
      if (isEmpty($col)) {
        $metadata[DATELABEL] = "Date Uploaded";
      }
      break;
    case GROUPNAME:
      // verify groupname is valid for this project
      // if valid, get contid as groupid metadata
      // Treat blank groupname as null
      if (isEmpty($col)) {
        $metadata[GROUPNAME] = null;
      } else {
        $rc = valid_groupname($col);
        if ($rc === False) {
          throw new Exception("Invalid groupname $col for project $projectid");
        } else {
          $metadata[GROUPID] = $rc;
        }
      }
      break;
    case SUBGROUPNAME:
      // verify subgroupname is valid for this project and groupname
      // if valid, get contid as subgroupid metadata
      // Treat blank subgroupname as null
      if (isEmpty($col)) {
        $metadata[SUBGROUPNAME] = null;
      } else {
        $groupname = $metadata[GROUPNAME];
        if (isEmpty($groupname)) {
          throw new Exception("Cannot specify subgroupname $col without groupname, project $projectid");
        }
        $rc = valid_subgroupname($groupname, $col);
        if ($rc === False) {
          throw new Exception("Invalid subgroupname $col for group $groupname, project $projectid");
        } else {
          $metadata[SUBGROUPID] = $rc;
        }
      }
      break;
    case DOCSUMMARY:
      // docsummary len <= 500
      if (strlen($col) > MAX_DOCSUMMARY_LEN) {
        throw new Exception("Document summary too long, > " . MAX_DOCSUMMARY_LEN);
      }
      break;
    case RECIPIENT:
      break;
    case DATECREATED:
      // check format
      if (!isEmpty($col)) {
        if (!is_numeric($col)) {
          throw new Exception("Date created $col is not a dateserial, check type in spreadsheet");
        }
      }
      break;
    case SENSITIVE:
      // Convert from string to boolean
      // Default to False
      if (isEmpty($col)) {
        $sensitiveFlag = False;
      } else {
        $val = (strtolower(trim($col)));
        switch ($val) {
          case 'yes':
            $sensitiveFlag = True;
            break;
          case 'no':
            $sensitiveFlag = False;
            break;
          default:
            throw new Exception("Bad value for sensitiveflag, must be Yes/No");
        }
        $metadata[SENSITIVE] = $sensitiveFlag;
      }
      break;
    case SENSITIVITYRATIONALE:
      // Only validate if sensitiveFlag is set to True
      if ($sensitiveFlag) {
        if (!valid_sensitivityrationale($col)) {
          throw new Exception("Sensitivity rationale $col is invalid");
        }
      }
      break;
    case DOCUMENTAUTHOR:
      break;
    }
  }
  return $metadata;
}

/*
 *	If cell is null, zero-length, or blank, treat as empty
 */
function isEmpty($val) {
  return (($val == null) || (trim($val) == ""));
}

/*
 * Reformat dateserial to "docdate" format used by swftarget.php (MM/DD/YYYY)
 *
 * PHPExcel_Shared_Date::ExcelToPHP assumes GMT so dates are usually
 * off by 1 unless you add in your time zone difference
 */
function dateserialToDocdate($dateserial) {
      $phpdate = PHPExcel_Shared_Date::ExcelToPHP($dateserial);

      // get local timezone
      $this_tz_str = date_default_timezone_get();	// America/New York
      $this_tz = new DateTimeZone($this_tz_str);	// local timezone

      // convert Unix timestamp to PHP datetime
      $dtime = new DateTime("@$phpdate", $this_tz);

      // get PHP datetime offset (seconds) from GMT
      $offset = $this_tz->getOffset($dtime);

      // adjust original timestamp to local timezone
      $phpdate -= $offset;

      return date("m/d/Y", $phpdate);
}

/*
 *	Validate groupname for this project
 *	Returns contid if valid
 *	Returns false if invalid
 */
function valid_groupname($groupname) {
  global $CONTAINERS_TREE;
  foreach ($CONTAINERS_TREE['contents'] as $category) {
    foreach ($category['contents'] as $group) {
      if ($group['label'] == $groupname) {
        return $group['contid'];
      }
    }
  }
  return false;
}

/*
 *	Validate subgroupname for this project and group
 *	Returns contid if valid
 *	Returns false if invalid
 */
function valid_subgroupname($groupname, $subgroupname) {
  global $CONTAINERS_TREE;
  foreach ($CONTAINERS_TREE['contents'] as $category) {
    foreach ($category['contents'] as $group) {
      if ($group['label'] == $groupname) {
        foreach ($group['contents'] as $subgroup) {
          if ($subgroup['label'] == $subgroupname) {
            return $subgroup['contid'];
          }
        }
      }
    }
  }
  return false;
}

/*
 *	Validate document sensitivity rationale
 */
function valid_sensitivityrationale($rationale) {
  global $RATIONALES_LIST;
  return in_array($rationale, $RATIONALES_LIST);
}

function getCell($worksheet, $row, $col) {
    return $worksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();
}

/*
 *	Record final status for an individual file
 *
 *	Note that unlike updateStatus, the row used here is
 *	the actual Excel row index
 */
function updateReport($row, $url, $msg) {
    global $REPORTFILE;
    $fp = fopen($REPORTFILE, "a+");
    if ($fp === False) {
      throw new Exception("Could not open report file $REPORTFILE");
    }
    $msg = quote_csv($msg);
    fwrite($fp, "$row,$url,$msg\n");
    fclose($fp);
}

/*
 *	Update job status
 *
 *	Note row and total should indicate number of rows processed
 *	and total number of rows, not just the Excel row index (which
 *	does not start from 1)
 */
function updateStatus($row, $total, $msg) {
    global $STATUSFILE;
    if (strlen($msg) == 0) {
      return;	// Don't wipe last status
    }
    $fp = fopen($STATUSFILE, "w+");
    if ($fp === False) {
      throw new Exception("Could not open status file $STATUSFILE");
    }
    $response = array(
      "row" => "$row",
      "total" => "$total",
      "msg" => "$msg"
    );
    fwrite($fp, json_encode($response));
    fclose($fp);
}

function quote_csv($msg) {
  $count = 0;
  $msg = preg_replace('/"/', '""', $msg, -1, $count);
  $msg = "\"$msg\"";
  return $msg;
}

/*
 *	Return without exception if job is open and available
 *	Also updates timestamp on job record (keepalive)
 */
function check_job($jobid) {
    $postfields = array("jobid" => $jobid);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, JOBUPDATE_TARGET);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    if ($result == "") {
      throw new Exception("No response from jobupdate service, check system configs?");
    }
    if ($result != "0") {
      throw new Exception("Job $jobid not available, cannot continue!");
    }
    curl_close ($ch);
}

function close_job($jobid) {
    $postfields = array("jobid" => $jobid);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, JOBCLOSE_TARGET);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close ($ch);
    /* DEBUG: Failing to close the job is probably bad but not fatal? */
}

?>
