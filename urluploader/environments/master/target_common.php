<?php
// Use the same script as the regular uploader to check in files
define('CHECKIN_TARGET', "https://data.ecosystem-management.org/uploader/swftarget.php");

// Script to update open job timestamps
define('JOBUPDATE_TARGET', "https://data.ecosystem-management.org/uploader/jobupdate.php");
define('JOBCLOSE_TARGET', "https://data.ecosystem-management.org/uploader/jobclose.php");

// Field name for submitted file
define('FILEIDX', "Filedata");

// File name for local copy of submitted file
define('EXCEL_FNAME', "urls.xlsx");

// Status response codes from swftarget
define('SWF_OK',	0);
define('SWF_ERR',	1);

// Column indices for the Excel spreadsheet
define('DOCUMENTURL',		0);
define('DOCTYPE',		1);
define('TITLE',			2);
define('DOCDATE',		3);
define('DATELABEL',		4);
define('GROUPNAME',		5);
define('SUBGROUPNAME',		6);
define('DOCSUMMARY',		7);
define('RECIPIENT',		8);
define('DATECREATED',		9);
define('SENSITIVE',		10);
define('SENSITIVITYRATIONALE',	11);
define('DOCUMENTAUTHOR',	12);
// More index numbers for additional metadata fields
define('JOBID',			13);
define('APPSYSID',		14);
define('AUTHOR',		15);
define('PUBDATE',		16);
define('PROJECTID',		17);
define('FILENAME',		18);
define('GROUPID',		19);
define('SUBGROUPID',		20);
// Still more metadata
define('ROW',			21);

// Metadata validation limits, matches similar values in the broker
define('MAX_FILENAME_LEN',	255);
define('MAX_TITLE_LEN',		200);
define('MAX_AUTHOR_LEN',	80);
define('MAX_DOCSUMMARY_LEN',	500);
define('PROJECT_TYPE',		'NEPA');

// Job data is stored in /var/www/html/tmp/urluploader/[jobid]/
define('TMPDIR',		'/var/www/html/tmp/urluploader');

function init_globals($jobid) {
  global $WWWJOBDIR;
  global $REPORTFILE;
  global $REPORTFILE_URL;
  global $STATUSFILE;
  global $STATUSFILE_URL;
  global $CONTAINERS_TREE;
  global $RATIONALES_LIST;
  global $EXCEL_FILE;

  // Job data is stored in /var/www/html/tmp/urluploader/[jobid]/
  $WWWJOBDIR = TMPDIR . "/$jobid";	// working directory
  $REPORTFILE = "$WWWJOBDIR/report.csv";

  // URL for the "get" script to return report CSVs with correct MIME type
  $REPORTFILE_URL = "../docs/get.php?doc=urlupreport&jobid=$jobid";

  // For the status file, omit the host portion of the URL because
  // it causes errors due to the JS same-source policy
  // Also don't use a .js suffix because the modified MIME-type causes
  // jQuery to interpret the response incorrectly
  $STATUSFILE = "$WWWJOBDIR/status.txt";		// contains JSON
  $STATUSFILE_URL = "/tmp/urluploader/$jobid/status.txt";// omit host for
							// same-source policy
  $EXCEL_FILE = "$WWWJOBDIR/" . EXCEL_FNAME;            // Excel spreadsheet
  $CONTAINERS_TREE = array();                           // more globals
  $RATIONALES_LIST = array();
}

?>
