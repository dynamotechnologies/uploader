<?php
/*
 *	This is the target script for the URL uploader
 *
 *	No more progress bar!  Announce that email will be forthcoming
 *	Submit the uploaded spreadsheet to target_svc.php for processing
 */

require_once("target_common.php");

$URL = "/uploader/urluploader/target_svc.php";
$FATALERROR = "";	// Set to non-empty string to flag a validation error

// Validate POST parameters

if (!isset($_POST['jobid'])) { $FATALERROR = "jobid missing!"; }
if (!isset($_POST['author'])) { $FATALERROR = "author missing!"; }
if (!isset($_POST['appsysid'])) { $FATALERROR = "appsysid missing!"; }
if (!isset($_POST['projectid'])) { $FATALERROR = "projectid missing!"; }
if (!isset($_POST['email'])) { $FATALERROR = "email missing!"; }

/*
 *	Initialize working directory, copy uploaded Excel file
 *
 *	Job monitoring starts in jQuery(document).ready() event handler
 */
try {

  $jobid = $_POST['jobid'];
  init_globals($jobid);		// from target_common.php

  /*
   *    Create working dir
   */
  if (!is_dir($WWWJOBDIR)) {
      if (!mkdir($WWWJOBDIR, 0777, True)) {
          throw new Exception("Unable to create job dir $WWWJOBDIR!");
      }
  }

  // Validate file (but not file contents)
  fileCheck(FILEIDX);

  // Check Excel file extension (must be .xlsx)
  $excelname=$_FILES[FILEIDX]['name'];
  if ($excelname == '') {
    throw new Exception("No Excel file submitted?");
  }
  if (end(explode('.', $excelname)) != 'xlsx') {
    throw new Exception("Bad filetype for Excel file $excelname!");
  }

  // Move file to job directory
  $tmpname=$_FILES[FILEIDX]['tmp_name'];
  if (!move_uploaded_file($tmpname, $EXCEL_FILE)) {
    throw new Exception("Unable to move Excel file $excelname to $EXCEL_FILE!");
  }

  // Fall through and display jQuery interface
  
} catch (Exception $e) {

  // Flag unrecoverable error
  $FATALERROR = "Error: " . $e->getMessage() . " Code: " . $e->getCode();

}

function fileCheck($fileidx) {
  $uploadData = $_FILES[$fileidx];
  $errCode = $uploadData['error'];
  if ($errCode != UPLOAD_ERR_OK) {
    $errMsg = "Error message goes here";
    switch ($errCode) {
      case UPLOAD_ERR_INI_SIZE:
        $errMsg = "The uploaded file exceeds the upload_max_filesize directive (" .
          ini_get("upload_max_filesize") . ") in php.ini.";
        break;
      case UPLOAD_ERR_FORM_SIZE:
        $errMsg = "The uploaded file exceeds the MAX_FILE_SIZE directive that " .
          "was specified in the HTML form.";
        break;
      case UPLOAD_ERR_PARTIAL:
        $errMsg = "The uploaded file was only partially uploaded.";
        break;
      case UPLOAD_ERR_NO_FILE:
        $errMsg = "No file was uploaded.";
        break;
      case UPLOAD_ERR_NO_TMP_DIR:
        $errMsg = "Missing a temporary folder.";
        break;
      case UPLOAD_ERR_CANT_WRITE:
        $errMsg = "Failed to write file to disk";
        break;
      default:
        $errMsg = "Unrecognized error code ${errCode}";
    }
    throw new Exception($errMsg);
  }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link type="text/css" href="../jquery/css/custom-theme/jquery-ui-1.8.18.custom.css" rel="Stylesheet" />
<link rel="stylesheet" type="text/css" href="target.css"/>
<title></title>
</head>

<body>
<br/>
<br/>
<div id="heading">STATUS</div>
<br/>
<br/>
<div id="status">
<p>Your job has been submitted for processing.  An email will be sent to
<?php echo htmlspecialchars($_POST['email']); ?> when your job is complete.</p>
</div>
<div id="text2">
<p>Your job ID is <?php echo htmlspecialchars($_POST['jobid']); ?></p>
<p>You may now close this window.</p>
</div>
<br/>
<br/>
<br/>
<br/>
<div id="exitbtnbar">
<div id="exitbtn">Exit</div>
</div>

<script type="text/javascript" src="../jquery/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../jquery/js/jquery-ui-1.8.18.custom.min.js"></script>

<script type="text/javascript">
function setStatus(msg) {
        return $('#status').html(msg);
}

/*
 *	Main jQuery(document).ready() handler starts here
 */
$(function () {
	// Add window-close button (shown after job completes)
	$('#exitbtn').button().addClass('btn');
	$('#exitbtn').click(function() {
		window.close();
	});

	// Show error and quit if the job was submitted improperly
	fatalerror = "<?php echo $FATALERROR; ?>";
	if (fatalerror !== "") {
		setStatus(fatalerror).addClass('errortxt');
		return;
	}

	var postdata = {
		'jobid' : '<?php echo $_POST['jobid']; ?>',
		'author' : '<?php echo $_POST['author']; ?>',
		'appsysid' : '<?php echo $_POST['appsysid']; ?>',
		'projectid' : '<?php echo $_POST['projectid']; ?>',
		'email' : '<?php echo $_POST['email']; ?>'
	};

	// Start job, submit request to target_svc.php
	$.post('<?php echo $URL; ?>', postdata);
});
</script>

</body>

</html>
