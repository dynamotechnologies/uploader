<?php
/*
 *	Remove directories from /var/www/html/tmp/urluploader that are
 *	more than 1 week old
 */
require_once("target_common.php");

$cutoff_time = strtotime('-1 week');

if ($handle = opendir(TMPDIR)) {
	clearstatcache();
	while (false !== ($fname = readdir($handle))) {
   		if ($fname != "." && $fname != "..") {
			$fqfname = TMPDIR . "/$fname";
			if (	is_dir($fqfname) &&
				filemtime($fqfname) < $cutoff_time) {
				purgejobdir($fqfname);
			}
   		}
	}
  	closedir($handle);
}

function purgejobdir($jobdir) {
	foreach (scandir($jobdir) as $file) {
		if (($file == '.') || ($file == '..')) { continue; }
		$fqfile = "$jobdir/$file";
		echo "Deleting file $fqfile\n";
		unlink($fqfile);
	}
	echo "Deleting $jobdir\n";
	rmdir($jobdir);
}

